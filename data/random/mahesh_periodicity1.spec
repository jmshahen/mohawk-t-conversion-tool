Query: t0, role1
Expected: REACHABLE
CanAssign {
    <TRUE, t1, TRUE, t0, role1>
    <TRUE, t1, TRUE, t0, role2>
}

CanRevoke {}

CanEnable {}

CanDisable {}