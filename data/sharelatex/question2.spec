/* 
 * Question 2: Is time considered periodic?
 * 
 * Roles: role1, role2, role3
 * Time-Slots: t0
 * 
 * rule 3 -> assign role 1 for t1 ->
 * rule 2 -> assign role 2 to anyone with role 1 ->
 * rule 1 -> assign role 3 to anyone with role 2
 */
 
Query: t2,[role3]
Expected: TRUE

CanAssign {
    /* <Admin role, 
     * starting timeslot - end timeslot for admin role, 
     * role preconditions, 
     * [time slot rule gives role to user for, another time slot (optional)...],
     * role to give to the user>
     */
    <TRUE,t0,role2,t2,role3>                //(1)
    <TRUE,t1,role1,t0,role2>                //(2)
    <TRUE,t2,TRUE,t1,role1>                 //(3)
}

CanRevoke {}

CanEnable {}

CanDisable {}
