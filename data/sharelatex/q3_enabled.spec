/* 
 * Question 3: 
 * 
 * Roles: role1, role2, role3
 * Time-Slots: t0, t1
 * 
 * If the roles are enabled by default then this example with succeed
 */
 
Query: t0,[role3]
Expected: TRUE

CanAssign {
    /* <Admin role, 
     * starting timeslot - end timeslot for admin role, 
     * role preconditions, 
     * [time slot rule gives role to user for, another time slot (optional)...],
     * role to give to the user>
     */
    <TRUE,t0,TRUE,t0,role2>           //(1)
    <TRUE,t0,role2,t0,role3>          //(2)
}

CanRevoke {}

CanEnable {}

CanDisable {}
