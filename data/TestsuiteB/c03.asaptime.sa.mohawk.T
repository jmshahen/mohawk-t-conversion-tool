/* Generated On        : 2015/02/07 20:24:54.396
 * Generated With      : Mohawk Reverse Converter: ASAPTime SA
 * Number of Roles     : 32
 * Number of Timeslots : 10
 * Number of Rules     : 325
 * 
 * Roles     : |Size=32| [role1, role2, role3, role4, role5, role6, role7, role8, role9, role10, role11, role12, role13, role14, role15, role16, role17, role18, role19, role20, role21, role22, role23, role24, role25, role26, role27, role28, role29, role30, role31, role32]
 * Timeslots : |Size=10| [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t5, [role8]

Expected: UNKNOWN

/* 
 * Number of Rules       : 325
 * Largest Precondition  : 4
 * Largest Role Schedule : 1
 * Startable Rules       : 14
 * Truly Startable Rules : 14
 */
CanAssign {
    /*   1 */  <TRUE, t4, TRUE, t8, role1>
    /*   2 */  <TRUE, t8, TRUE, t3, role1>
    /*   3 */  <TRUE, t5, TRUE, t1, role1>
    /*   4 */  <TRUE, t3, role12, t7, role2>
    /*   5 */  <TRUE, t2, role12, t8, role2>
    /*   6 */  <TRUE, t6, role12, t9, role2>
    /*   7 */  <TRUE, t4, role12, t3, role2>
    /*   8 */  <TRUE, t6, role19, t1, role2>
    /*   9 */  <TRUE, t2, role30, t8, role2>
    /*  10 */  <TRUE, t9, role26, t10, role2>
    /*  11 */  <TRUE, t8, role26, t6, role2>
    /*  12 */  <TRUE, t6, role26, t7, role2>
    /*  13 */  <TRUE, t3, role26, t8, role2>
    /*  14 */  <TRUE, t2, role29 & NOT ~ role6 & NOT ~ role8 & NOT ~ role3, t7, role2>
    /*  15 */  <TRUE, t7, role29 & NOT ~ role6 & NOT ~ role8 & NOT ~ role3, t8, role2>
    /*  16 */  <TRUE, t8, role29 & NOT ~ role6 & NOT ~ role8 & NOT ~ role3, t6, role2>
    /*  17 */  <TRUE, t4, role29 & NOT ~ role6 & NOT ~ role8 & NOT ~ role3, t4, role2>
    /*  18 */  <TRUE, t4, role27 & NOT ~ role8 & NOT ~ role1 & NOT ~ role5, t7, role2>
    /*  19 */  <TRUE, t5, role27 & NOT ~ role8 & NOT ~ role1 & NOT ~ role5, t2, role2>
    /*  20 */  <TRUE, t10, role27, t8, role2>
    /*  21 */  <TRUE, t7, role12, t4, role2>
    /*  22 */  <TRUE, t8, role19, t6, role3>
    /*  23 */  <TRUE, t6, role19, t9, role3>
    /*  24 */  <TRUE, t4, role19, t2, role3>
    /*  25 */  <TRUE, t9, role15 & NOT ~ role2 & NOT ~ role6 & NOT ~ role2, t4, role4>
    /*  26 */  <TRUE, t4, role15 & NOT ~ role2 & NOT ~ role6 & NOT ~ role2, t7, role4>
    /*  27 */  <TRUE, t5, role7, t8, role4>
    /*  28 */  <TRUE, t7, role7, t10, role4>
    /*  29 */  <TRUE, t2, role7, t9, role4>
    /*  30 */  <TRUE, t10, role7, t6, role4>
    /*  31 */  <TRUE, t6, role1, t5, role4>
    /*  32 */  <TRUE, t7, role1, t1, role4>
    /*  33 */  <TRUE, t10, role1, t3, role4>
    /*  34 */  <TRUE, t4, role1, t6, role4>
    /*  35 */  <TRUE, t5, role21, t10, role4>
    /*  36 */  <TRUE, t2, role21, t8, role4>
    /*  37 */  <TRUE, t2, role29, t5, role4>
    /*  38 */  <TRUE, t3, role29, t9, role4>
    /*  39 */  <TRUE, t9, role29, t3, role4>
    /*  40 */  <TRUE, t4, role29, t1, role4>
    /*  41 */  <TRUE, t6, role27 & NOT ~ role6 & NOT ~ role3 & NOT ~ role8, t9, role4>
    /*  42 */  <TRUE, t5, role27 & NOT ~ role6 & NOT ~ role3 & NOT ~ role8, t10, role4>
    /*  43 */  <TRUE, t2, role27 & NOT ~ role6 & NOT ~ role3 & NOT ~ role8, t7, role4>
    /*  44 */  <TRUE, t7, role27 & NOT ~ role6 & NOT ~ role3 & NOT ~ role8, t4, role4>
    /*  45 */  <TRUE, t1, role21 & NOT ~ role6 & NOT ~ role7 & NOT ~ role6, t4, role4>
    /*  46 */  <TRUE, t8, role21 & NOT ~ role6 & NOT ~ role7 & NOT ~ role6, t10, role4>
    /*  47 */  <TRUE, t7, role30 & NOT ~ role5 & NOT ~ role7 & NOT ~ role6, t2, role4>
    /*  48 */  <TRUE, t8, role30 & NOT ~ role5 & NOT ~ role7 & NOT ~ role6, t9, role4>
    /*  49 */  <TRUE, t2, role30 & NOT ~ role5 & NOT ~ role7 & NOT ~ role6, t6, role4>
    /*  50 */  <TRUE, t9, role30 & NOT ~ role5 & NOT ~ role7 & NOT ~ role6, t3, role4>
    /*  51 */  <TRUE, t6, role5, t7, role4>
    /*  52 */  <TRUE, t7, role5, t3, role4>
    /*  53 */  <TRUE, t8, role5, t4, role4>
    /*  54 */  <TRUE, t4, role5, t5, role4>
    /*  55 */  <TRUE, t1, role16 & NOT ~ role3 & NOT ~ role4 & NOT ~ role4, t1, role5>
    /*  56 */  <TRUE, t7, role16 & NOT ~ role3 & NOT ~ role4 & NOT ~ role4, t4, role5>
    /*  57 */  <TRUE, t8, role16 & NOT ~ role3 & NOT ~ role4 & NOT ~ role4, t3, role5>
    /*  58 */  <TRUE, t5, role16 & NOT ~ role3 & NOT ~ role4 & NOT ~ role4, t5, role5>
    /*  59 */  <TRUE, t4, role28, t7, role6>
    /*  60 */  <TRUE, t5, role28, t9, role6>
    /*  61 */  <TRUE, t3, role18, t9, role6>
    /*  62 */  <TRUE, t4, role18, t7, role6>
    /*  63 */  <TRUE, t5, role18, t2, role6>
    /*  64 */  <TRUE, t8, role21 & NOT ~ role8 & NOT ~ role7 & NOT ~ role2, t1, role6>
    /*  65 */  <TRUE, t10, role28, t5, role6>
    /*  66 */  <TRUE, t6, role21 & NOT ~ role1 & NOT ~ role5 & NOT ~ role5, t3, role6>
    /*  67 */  <TRUE, t5, role25 & NOT ~ role2 & NOT ~ role5 & NOT ~ role3, t7, role6>
    /*  68 */  <TRUE, t2, role25 & NOT ~ role2 & NOT ~ role5 & NOT ~ role3, t9, role6>
    /*  69 */  <TRUE, t6, role25 & NOT ~ role2 & NOT ~ role5 & NOT ~ role3, t8, role6>
    /*  70 */  <TRUE, t7, role25 & NOT ~ role2 & NOT ~ role5 & NOT ~ role3, t10, role6>
    /*  71 */  <TRUE, t4, role22 & NOT ~ role8 & NOT ~ role8 & NOT ~ role4, t3, role6>
    /*  72 */  <TRUE, t3, role22 & NOT ~ role8 & NOT ~ role8 & NOT ~ role4, t9, role6>
    /*  73 */  <TRUE, t6, role22 & NOT ~ role8 & NOT ~ role8 & NOT ~ role4, t1, role6>
    /*  74 */  <TRUE, t10, role22 & NOT ~ role8 & NOT ~ role8 & NOT ~ role4, t2, role6>
    /*  75 */  <TRUE, t2, role10, t8, role6>
    /*  76 */  <TRUE, t4, role12, t6, role6>
    /*  77 */  <TRUE, t10, role12, t4, role6>
    /*  78 */  <TRUE, t3, role12, t5, role6>
    /*  79 */  <TRUE, t7, role26, t2, role6>
    /*  80 */  <TRUE, t2, role26, t8, role6>
    /*  81 */  <TRUE, t6, role25, t3, role7>
    /*  82 */  <TRUE, t5, role25, t4, role7>
    /*  83 */  <TRUE, t7, role28, t4, role7>
    /*  84 */  <TRUE, t8, role28, t1, role7>
    /*  85 */  <TRUE, t3, role25, t4, role7>
    /*  86 */  <TRUE, t8, role25, t2, role7>
    /*  87 */  <TRUE, t1, role25, t1, role7>
    /*  88 */  <TRUE, t6, role28 & NOT ~ role2 & NOT ~ role6 & NOT ~ role8, t1, role7>
    /*  89 */  <TRUE, t5, role28 & NOT ~ role2 & NOT ~ role6 & NOT ~ role8, t3, role7>
    /*  90 */  <TRUE, t1, role28 & NOT ~ role2 & NOT ~ role6 & NOT ~ role8, t2, role7>
    /*  91 */  <TRUE, t9, role28 & NOT ~ role2 & NOT ~ role6 & NOT ~ role8, t10, role7>
    /*  92 */  <TRUE, t6, role12, t1, role7>
    /*  93 */  <TRUE, t2, role23, t10, role7>
    /*  94 */  <TRUE, t6, role23, t2, role7>
    /*  95 */  <TRUE, t8, role23, t9, role7>
    /*  96 */  <TRUE, t2, role24, t9, role7>
    /*  97 */  <TRUE, t1, role24, t5, role7>
    /*  98 */  <TRUE, t3, role24, t4, role7>
    /*  99 */  <TRUE, t10, role24, t1, role7>
    /* 100 */  <TRUE, t7, TRUE, t9, role7>
    /* 101 */  <TRUE, t10, role19 & NOT ~ role1 & NOT ~ role5 & NOT ~ role5, t6, role7>
    /* 102 */  <TRUE, t4, role11, t5, role8>
    /* 103 */  <TRUE, t5, role19 & role12, t5, role11>
    /* 104 */  <TRUE, t4, role1, t5, role13>
    /* 105 */  <TRUE, t3, role1, t5, role14>
    /* 106 */  <TRUE, t4, role1, t5, role15>
    /* 107 */  <TRUE, t4, role1, t5, role16>
    /* 108 */  <TRUE, t5, role14, t5, role21>
    /* 109 */  <TRUE, t4, role19, t5, role11>
    /* 110 */  <TRUE, t1, role19 & NOT ~ role31, t5, role12>
    /* 111 */  <TRUE, t3, role13, t5, role13>
    /* 112 */  <TRUE, t4, role13, t5, role14>
    /* 113 */  <TRUE, t3, role13, t5, role15>
    /* 114 */  <TRUE, t5, role13, t5, role16>
    /* 115 */  <TRUE, t1, role19, t5, role17>
    /* 116 */  <TRUE, t2, role13, t5, role18>
    /* 117 */  <TRUE, t3, role12 & NOT ~ role11, t5, role31>
    /* 118 */  <TRUE, t4, role11 & NOT ~ role12, t5, role31>
    /* 119 */  <TRUE, t1, role14 & NOT ~ role13, t5, role31>
    /* 120 */  <TRUE, t2, role13 & NOT ~ role14, t5, role31>
    /* 121 */  <TRUE, t2, role16 & NOT ~ role15, t5, role31>
    /* 122 */  <TRUE, t3, role15 & NOT ~ role16, t5, role31>
    /* 123 */  <TRUE, t5, role20 & NOT ~ role19 & NOT ~ role18, t5, role31>
    /* 124 */  <TRUE, t3, role31 & NOT ~ role21, t5, role12>
    /* 125 */  <TRUE, t5, role21, t5, role15>
    /* 126 */  <TRUE, t4, TRUE, t5, role1>
    /* 127 */  <TRUE, t2, TRUE, t5, role19>
    /* 128 */  <TRUE, t3, role25 & NOT ~ role5 & NOT ~ role4 & NOT ~ role8, t7, role9>
    /* 129 */  <TRUE, t5, role25 & NOT ~ role5 & NOT ~ role4 & NOT ~ role8, t2, role9>
    /* 130 */  <TRUE, t6, role25 & NOT ~ role5 & NOT ~ role4 & NOT ~ role8, t9, role9>
    /* 131 */  <TRUE, t7, role1, t10, role10>
    /* 132 */  <TRUE, t8, role1, t1, role10>
    /* 133 */  <TRUE, t7, role4 & NOT ~ role3 & NOT ~ role6 & NOT ~ role7, t7, role10>
    /* 134 */  <TRUE, t5, role4 & NOT ~ role3 & NOT ~ role6 & NOT ~ role7, t8, role10>
    /* 135 */  <TRUE, t8, role1, t3, role10>
    /* 136 */  <TRUE, t3, role29 & NOT ~ role5 & NOT ~ role8 & NOT ~ role3, t5, role10>
    /* 137 */  <TRUE, t9, role29 & NOT ~ role5 & NOT ~ role8 & NOT ~ role3, t8, role10>
    /* 138 */  <TRUE, t10, role23, t2, role10>
    /* 139 */  <TRUE, t10, role26, t8, role10>
    /* 140 */  <TRUE, t3, role26, t5, role10>
    /* 141 */  <TRUE, t1, role32 & NOT ~ role4 & NOT ~ role3 & NOT ~ role1, t6, role10>
    /* 142 */  <TRUE, t8, role32 & NOT ~ role4 & NOT ~ role3 & NOT ~ role1, t10, role10>
    /* 143 */  <TRUE, t4, role21 & NOT ~ role7 & NOT ~ role3 & NOT ~ role5, t3, role10>
    /* 144 */  <TRUE, t2, role6, t1, role10>
    /* 145 */  <TRUE, t10, role6, t9, role10>
    /* 146 */  <TRUE, t5, role4, t8, role12>
    /* 147 */  <TRUE, t3, role3, t8, role12>
    /* 148 */  <TRUE, t4, role3, t6, role12>
    /* 149 */  <TRUE, t10, role3, t9, role12>
    /* 150 */  <TRUE, t6, role10 & NOT ~ role7 & NOT ~ role8 & NOT ~ role4, t6, role12>
    /* 151 */  <TRUE, t2, role10 & NOT ~ role7 & NOT ~ role8 & NOT ~ role4, t7, role12>
    /* 152 */  <TRUE, t4, role10 & NOT ~ role7 & NOT ~ role8 & NOT ~ role4, t4, role12>
    /* 153 */  <TRUE, t1, role22 & NOT ~ role7 & NOT ~ role6 & NOT ~ role3, t9, role12>
    /* 154 */  <TRUE, t3, role22 & NOT ~ role7 & NOT ~ role6 & NOT ~ role3, t8, role12>
    /* 155 */  <TRUE, t7, role31, t8, role12>
    /* 156 */  <TRUE, t9, role19, t1, role12>
    /* 157 */  <TRUE, t4, role19, t8, role12>
    /* 158 */  <TRUE, t5, role19, t5, role12>
    /* 159 */  <TRUE, t10, role19, t6, role12>
    /* 160 */  <TRUE, t3, role29 & NOT ~ role3 & NOT ~ role5 & NOT ~ role8, t9, role12>
    /* 161 */  <TRUE, t9, role29 & NOT ~ role3 & NOT ~ role5 & NOT ~ role8, t3, role12>
    /* 162 */  <TRUE, t7, role18, t4, role12>
    /* 163 */  <TRUE, t3, role18, t6, role12>
    /* 164 */  <TRUE, t8, role18, t1, role12>
    /* 165 */  <TRUE, t10, role2, t2, role12>
    /* 166 */  <TRUE, t5, role2, t9, role12>
    /* 167 */  <TRUE, t3, role2, t3, role12>
    /* 168 */  <TRUE, t8, role2, t5, role12>
    /* 169 */  <TRUE, t9, role28, t9, role15>
    /* 170 */  <TRUE, t10, role28, t5, role15>
    /* 171 */  <TRUE, t6, role12 & NOT ~ role8 & NOT ~ role5 & NOT ~ role7, t5, role15>
    /* 172 */  <TRUE, t3, role12 & NOT ~ role8 & NOT ~ role5 & NOT ~ role7, t3, role15>
    /* 173 */  <TRUE, t2, role17, t6, role15>
    /* 174 */  <TRUE, t5, role17, t3, role15>
    /* 175 */  <TRUE, t7, role17, t4, role15>
    /* 176 */  <TRUE, t9, role3, t10, role15>
    /* 177 */  <TRUE, t5, role3, t1, role15>
    /* 178 */  <TRUE, t7, role3, t8, role15>
    /* 179 */  <TRUE, t5, role28, t2, role15>
    /* 180 */  <TRUE, t3, role28, t10, role15>
    /* 181 */  <TRUE, t8, role28, t5, role15>
    /* 182 */  <TRUE, t7, role29, t4, role15>
    /* 183 */  <TRUE, t9, role29, t10, role15>
    /* 184 */  <TRUE, t4, role29, t5, role15>
    /* 185 */  <TRUE, t7, role16, t10, role15>
    /* 186 */  <TRUE, t8, role16, t2, role15>
    /* 187 */  <TRUE, t8, TRUE, t7, role15>
    /* 188 */  <TRUE, t5, TRUE, t1, role15>
    /* 189 */  <TRUE, t3, TRUE, t9, role15>
    /* 190 */  <TRUE, t8, role25, t5, role16>
    /* 191 */  <TRUE, t9, role25, t9, role16>
    /* 192 */  <TRUE, t7, role2, t2, role16>
    /* 193 */  <TRUE, t4, role7, t1, role16>
    /* 194 */  <TRUE, t7, role7, t2, role16>
    /* 195 */  <TRUE, t2, role7, t7, role16>
    /* 196 */  <TRUE, t3, role7, t3, role16>
    /* 197 */  <TRUE, t5, role17, t4, role16>
    /* 198 */  <TRUE, t3, role17, t1, role16>
    /* 199 */  <TRUE, t4, role17, t2, role16>
    /* 200 */  <TRUE, t4, role28 & NOT ~ role2 & NOT ~ role7 & NOT ~ role4, t7, role16>
    /* 201 */  <TRUE, t1, role28 & NOT ~ role2 & NOT ~ role7 & NOT ~ role4, t3, role16>
    /* 202 */  <TRUE, t2, role28 & NOT ~ role2 & NOT ~ role7 & NOT ~ role4, t9, role16>
    /* 203 */  <TRUE, t3, role28 & NOT ~ role2 & NOT ~ role7 & NOT ~ role4, t10, role16>
    /* 204 */  <TRUE, t2, role3, t2, role16>
    /* 205 */  <TRUE, t2, role19 & NOT ~ role6 & NOT ~ role7 & NOT ~ role3, t8, role16>
    /* 206 */  <TRUE, t10, role19 & NOT ~ role6 & NOT ~ role7 & NOT ~ role3, t9, role16>
    /* 207 */  <TRUE, t1, role19 & NOT ~ role6 & NOT ~ role7 & NOT ~ role3, t2, role16>
    /* 208 */  <TRUE, t6, role9, t8, role16>
    /* 209 */  <TRUE, t4, role9 & NOT ~ role6 & NOT ~ role2 & NOT ~ role4, t9, role17>
    /* 210 */  <TRUE, t1, role9 & NOT ~ role6 & NOT ~ role2 & NOT ~ role4, t5, role17>
    /* 211 */  <TRUE, t5, role9 & NOT ~ role6 & NOT ~ role2 & NOT ~ role4, t8, role17>
    /* 212 */  <TRUE, t6, role28, t2, role18>
    /* 213 */  <TRUE, t3, role28, t6, role18>
    /* 214 */  <TRUE, t9, role28, t3, role18>
    /* 215 */  <TRUE, t1, role9, t5, role18>
    /* 216 */  <TRUE, t2, role9, t1, role18>
    /* 217 */  <TRUE, t10, role9, t6, role18>
    /* 218 */  <TRUE, t5, role19, t4, role18>
    /* 219 */  <TRUE, t8, role19, t7, role18>
    /* 220 */  <TRUE, t10, role27, t9, role18>
    /* 221 */  <TRUE, t7, role27, t8, role18>
    /* 222 */  <TRUE, t9, role27, t3, role18>
    /* 223 */  <TRUE, t2, role27, t4, role18>
    /* 224 */  <TRUE, t6, TRUE, t4, role18>
    /* 225 */  <TRUE, t1, TRUE, t10, role18>
    /* 226 */  <TRUE, t7, TRUE, t7, role18>
    /* 227 */  <TRUE, t10, role3, t4, role18>
    /* 228 */  <TRUE, t3, role3, t1, role18>
    /* 229 */  <TRUE, t2, role6, t2, role18>
    /* 230 */  <TRUE, t3, role6, t9, role18>
    /* 231 */  <TRUE, t9, role6, t10, role18>
    /* 232 */  <TRUE, t4, role16, t1, role18>
    /* 233 */  <TRUE, t3, role16, t7, role18>
    /* 234 */  <TRUE, t5, role16, t2, role18>
    /* 235 */  <TRUE, t7, TRUE, t1, role19>
    /* 236 */  <TRUE, t6, TRUE, t10, role19>
    /* 237 */  <TRUE, t1, role25 & NOT ~ role2 & NOT ~ role5 & NOT ~ role7, t1, role20>
    /* 238 */  <TRUE, t2, role25 & NOT ~ role2 & NOT ~ role5 & NOT ~ role7, t9, role20>
    /* 239 */  <TRUE, t3, role25 & NOT ~ role2 & NOT ~ role5 & NOT ~ role7, t5, role20>
    /* 240 */  <TRUE, t10, role25 & NOT ~ role2 & NOT ~ role5 & NOT ~ role7, t6, role20>
    /* 241 */  <TRUE, t5, role15, t5, role21>
    /* 242 */  <TRUE, t10, role15, t7, role21>
    /* 243 */  <TRUE, t7, role1, t1, role22>
    /* 244 */  <TRUE, t10, role1, t5, role22>
    /* 245 */  <TRUE, t3, role15 & NOT ~ role7 & NOT ~ role7 & NOT ~ role3, t7, role22>
    /* 246 */  <TRUE, t10, role15 & NOT ~ role7 & NOT ~ role7 & NOT ~ role3, t10, role22>
    /* 247 */  <TRUE, t5, role31, t4, role22>
    /* 248 */  <TRUE, t6, role19, t3, role22>
    /* 249 */  <TRUE, t3, role31, t3, role22>
    /* 250 */  <TRUE, t5, role31, t4, role22>
    /* 251 */  <TRUE, t4, role31, t5, role22>
    /* 252 */  <TRUE, t1, role31, t6, role22>
    /* 253 */  <TRUE, t9, role7 & NOT ~ role2 & NOT ~ role5 & NOT ~ role4, t6, role22>
    /* 254 */  <TRUE, t5, role7 & NOT ~ role2 & NOT ~ role5 & NOT ~ role4, t7, role22>
    /* 255 */  <TRUE, t3, role7 & NOT ~ role2 & NOT ~ role5 & NOT ~ role4, t9, role22>
    /* 256 */  <TRUE, t6, role7 & NOT ~ role2 & NOT ~ role5 & NOT ~ role4, t3, role22>
    /* 257 */  <TRUE, t10, role3, t4, role22>
    /* 258 */  <TRUE, t9, role3, t7, role22>
    /* 259 */  <TRUE, t6, role17, t3, role22>
    /* 260 */  <TRUE, t5, role17, t4, role22>
    /* 261 */  <TRUE, t7, role16 & NOT ~ role1 & NOT ~ role7 & NOT ~ role5, t10, role22>
    /* 262 */  <TRUE, t4, role16 & NOT ~ role1 & NOT ~ role7 & NOT ~ role5, t7, role22>
    /* 263 */  <TRUE, t8, role16 & NOT ~ role1 & NOT ~ role7 & NOT ~ role5, t1, role22>
    /* 264 */  <TRUE, t2, role16 & NOT ~ role1 & NOT ~ role7 & NOT ~ role5, t8, role22>
    /* 265 */  <TRUE, t8, role6, t1, role23>
    /* 266 */  <TRUE, t7, role6, t4, role23>
    /* 267 */  <TRUE, t5, role6, t3, role23>
    /* 268 */  <TRUE, t2, role6, t9, role23>
    /* 269 */  <TRUE, t5, role3, t2, role24>
    /* 270 */  <TRUE, t6, role3, t6, role24>
    /* 271 */  <TRUE, t7, role3, t10, role24>
    /* 272 */  <TRUE, t1, role7, t8, role25>
    /* 273 */  <TRUE, t6, role7, t3, role25>
    /* 274 */  <TRUE, t4, role7, t6, role25>
    /* 275 */  <TRUE, t5, role7, t7, role25>
    /* 276 */  <TRUE, t6, role18, t7, role26>
    /* 277 */  <TRUE, t4, role18, t5, role26>
    /* 278 */  <TRUE, t4, role7, t10, role27>
    /* 279 */  <TRUE, t1, role7, t5, role27>
    /* 280 */  <TRUE, t5, role23 & NOT ~ role5 & NOT ~ role8 & NOT ~ role6, t3, role28>
    /* 281 */  <TRUE, t8, role17 & NOT ~ role8 & NOT ~ role3 & NOT ~ role5, t8, role29>
    /* 282 */  <TRUE, t7, role17 & NOT ~ role8 & NOT ~ role3 & NOT ~ role5, t7, role29>
    /* 283 */  <TRUE, t6, role17 & NOT ~ role8 & NOT ~ role3 & NOT ~ role5, t2, role29>
    /* 284 */  <TRUE, t4, role20 & NOT ~ role3 & NOT ~ role2 & NOT ~ role8, t10, role29>
    /* 285 */  <TRUE, t9, role20 & NOT ~ role3 & NOT ~ role2 & NOT ~ role8, t5, role29>
    /* 286 */  <TRUE, t2, role22, t5, role29>
    /* 287 */  <TRUE, t3, role22, t4, role29>
    /* 288 */  <TRUE, t10, role3, t6, role29>
    /* 289 */  <TRUE, t1, role3, t3, role29>
    /* 290 */  <TRUE, t8, role3, t5, role29>
    /* 291 */  <TRUE, t2, role3, t7, role29>
    /* 292 */  <TRUE, t4, role1, t9, role29>
    /* 293 */  <TRUE, t8, role1, t10, role29>
    /* 294 */  <TRUE, t3, role1, t8, role29>
    /* 295 */  <TRUE, t5, role22, t6, role29>
    /* 296 */  <TRUE, t10, role22, t10, role29>
    /* 297 */  <TRUE, t3, role18, t7, role29>
    /* 298 */  <TRUE, t4, role18, t9, role29>
    /* 299 */  <TRUE, t5, role18, t3, role29>
    /* 300 */  <TRUE, t8, role18, t1, role29>
    /* 301 */  <TRUE, t10, role19 & NOT ~ role3 & NOT ~ role8 & NOT ~ role6, t4, role30>
    /* 302 */  <TRUE, t5, role19 & NOT ~ role3 & NOT ~ role8 & NOT ~ role6, t8, role30>
    /* 303 */  <TRUE, t3, role19 & NOT ~ role3 & NOT ~ role8 & NOT ~ role6, t10, role30>
    /* 304 */  <TRUE, t6, role19 & NOT ~ role3 & NOT ~ role8 & NOT ~ role6, t6, role30>
    /* 305 */  <TRUE, t3, role25, t8, role31>
    /* 306 */  <TRUE, t6, role25, t3, role31>
    /* 307 */  <TRUE, t7, role15 & NOT ~ role3 & NOT ~ role2 & NOT ~ role8, t3, role32>
    /* 308 */  <TRUE, t4, role15 & NOT ~ role3 & NOT ~ role2 & NOT ~ role8, t5, role32>
    /* 309 */  <TRUE, t8, role15 & NOT ~ role3 & NOT ~ role2 & NOT ~ role8, t4, role32>
    /* 310 */  <TRUE, t5, role15 & NOT ~ role3 & NOT ~ role2 & NOT ~ role8, t1, role32>
    /* 311 */  <TRUE, t1, role28, t5, role32>
    /* 312 */  <TRUE, t9, role28, t6, role32>
    /* 313 */  <TRUE, t2, role28, t1, role32>
    /* 314 */  <TRUE, t9, role4, t4, role32>
    /* 315 */  <TRUE, t5, role26 & NOT ~ role2 & NOT ~ role8 & NOT ~ role2, t10, role32>
    /* 316 */  <TRUE, t6, role21 & NOT ~ role7 & NOT ~ role4 & NOT ~ role5, t10, role32>
    /* 317 */  <TRUE, t4, role21 & NOT ~ role7 & NOT ~ role4 & NOT ~ role5, t5, role32>
    /* 318 */  <TRUE, t10, role21 & NOT ~ role7 & NOT ~ role4 & NOT ~ role5, t1, role32>
    /* 319 */  <TRUE, t3, role16 & NOT ~ role6 & NOT ~ role2 & NOT ~ role3, t7, role32>
    /* 320 */  <TRUE, t7, role16 & NOT ~ role6 & NOT ~ role2 & NOT ~ role3, t2, role32>
    /* 321 */  <TRUE, t6, role16 & NOT ~ role6 & NOT ~ role2 & NOT ~ role3, t6, role32>
    /* 322 */  <TRUE, t2, role29 & NOT ~ role2 & NOT ~ role1 & NOT ~ role1, t7, role32>
    /* 323 */  <TRUE, t3, role29 & NOT ~ role2 & NOT ~ role1 & NOT ~ role1, t9, role32>
    /* 324 */  <TRUE, t1, role15, t7, role32>
    /* 325 */  <TRUE, t8, role15, t8, role32>
}

CanRevoke { }

CanEnable { }

CanDisable { }