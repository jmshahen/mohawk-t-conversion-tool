/* everything after a //, until the newline, is considered a comment and ignored
 *
 * white space is ignored, this includes tabs and spaces and newline characters
 * You      can put as much      white space anywhere and it will be ignored
 * 
 * 
 * The order of the document doesn't matter: Query can be last, first, or somewhere in the middle and same goes for all the others
 */
CanAssign {
    // <Admin role, starting timeslot - end timeslot for admin role, role preconditions, [time slot rule gives role to user for, another time slot (optional)...],role to give to the user>
    <role0,t0-t5,TRUE,[t1,t1],role1>		                    //(1)
    <role0,t0-t0,role1 & NOT~ role2 & role3,[t1,t2],role2>		//(2)
}

CanRevoke {
    // same as above
}

CanEnable {
    // same as above
}

CanDisable {
    // same as above
}

// Expected solution: {REACHABLE, UNREACHABLE, UNKNOWN}; where for TRUE is means that we can achieve the Query
Expected: UNKNOWN

// Query: timeslot to check, [set of roles that the user must have in the timeslot (no NOTs allowed)]
Query: t0,[role1,role0,role2]