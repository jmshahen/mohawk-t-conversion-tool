/* Generated On        : 2015/02/07 20:24:43.627
 * Generated With      : Mohawk Reverse Converter: ASAPTime NSA
 * Number of Roles     : 33
 * Number of Timeslots : 5
 * Number of Rules     : 224
 * 
 * Roles     : |Size=33| [role1, role2, role3, role4, role5, role6, role7, role8, role9, role10, role11, role12, role13, role14, role15, role16, role17, role18, role19, role20, role21, role22, role23, role24, role25, role26, role27, role28, role29, role30, role32, role33, role34]
 * Timeslots : |Size=5| [t1, t2, t3, t4, t5]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t2, [role7]

Expected: UNKNOWN

/* 
 * Number of Rules       : 150
 * Largest Precondition  : 5
 * Largest Role Schedule : 1
 * Startable Rules       : 46
 * Truly Startable Rules : 10
 */
CanAssign {
    /*   1 */  <role1, t1, role14 & role32, t4, role34>
    /*   2 */  <role1, t5, role14 & role32, t5, role34>
    /*   3 */  <role1, t2, role14 & role32, t2, role34>
    /*   4 */  <TRUE, t3, role14 & role32, t1, role34>
    /*   5 */  <role22, t5, TRUE, t5, role23>
    /*   6 */  <role22, t2, TRUE, t4, role23>
    /*   7 */  <TRUE, t4, TRUE, t2, role28>
    /*   8 */  <TRUE, t4, TRUE, t4, role7>
    /*   9 */  <TRUE, t5, TRUE, t5, role7>
    /*  10 */  <role22, t3, TRUE, t4, role21>
    /*  11 */  <role22, t2, TRUE, t1, role21>
    /*  12 */  <TRUE, t4, TRUE, t2, role21>
    /*  13 */  <role22, t5, TRUE, t5, role21>
    /*  14 */  <role22, t1, TRUE, t3, role21>
    /*  15 */  <role22, t3, TRUE, t2, role3>
    /*  16 */  <TRUE, t2, TRUE, t4, role3>
    /*  17 */  <role22, t4, TRUE, t5, role3>
    /*  18 */  <role22, t5, TRUE, t4, role4>
    /*  19 */  <role22, t1, TRUE, t3, role4>
    /*  20 */  <TRUE, t1, TRUE, t2, role23>
    /*  21 */  <role22, t5, TRUE, t3, role23>
    /*  22 */  <role22, t4, TRUE, t1, role23>
    /*  23 */  <role22, t3, TRUE, t4, role23>
    /*  24 */  <role22, t1, TRUE, t3, role28>
    /*  25 */  <role22, t5, TRUE, t1, role28>
    /*  26 */  <role22, t2, TRUE, t2, role28>
    /*  27 */  <role22, t3, TRUE, t4, role28>
    /*  28 */  <role22, t4, TRUE, t5, role28>
    /*  29 */  <TRUE, t4, TRUE, t2, role7>
    /*  30 */  <role22, t2, TRUE, t3, role7>
    /*  31 */  <TRUE, t5, TRUE, t1, role7>
    /*  32 */  <role22, t1, TRUE, t5, role7>
    /*  33 */  <role22, t3, TRUE, t4, role7>
    /*  34 */  <role22, t4, TRUE, t3, role21>
    /*  35 */  <role22, t3, TRUE, t4, role21>
    /*  36 */  <role22, t5, TRUE, t4, role3>
    /*  37 */  <role22, t3, TRUE, t5, role3>
    /*  38 */  <role22, t4, TRUE, t1, role3>
    /*  39 */  <role22, t3, TRUE, t3, role4>
    /*  40 */  <role22, t5, TRUE, t5, role4>
    /*  41 */  <TRUE, t4, TRUE, t4, role4>
    /*  42 */  <role22, t1, TRUE, t1, role4>
    /*  43 */  <role22, t2, TRUE, t2, role4>
    /*  44 */  <role2, t3, NOT ~ role14, t5, role32>
    /*  45 */  <TRUE, t1, NOT ~ role14, t1, role32>
    /*  46 */  <role2, t2, NOT ~ role14, t2, role32>
    /*  47 */  <role8, t4, role14, t1, role30>
    /*  48 */  <TRUE, t2, role14, t3, role30>
    /*  49 */  <role8, t1, role14, t5, role30>
    /*  50 */  <role8, t5, role14, t2, role30>
    /*  51 */  <TRUE, t3, role14, t4, role30>
    /*  52 */  <role8, t4, role14, t2, role17>
    /*  53 */  <role8, t2, role14, t3, role17>
    /*  54 */  <role8, t4, role17, t2, role16>
    /*  55 */  <role8, t2, role17, t4, role16>
    /*  56 */  <role8, t1, role17, t5, role16>
    /*  57 */  <role8, t4, TRUE, t2, role7>
    /*  58 */  <TRUE, t1, role14, t4, role18>
    /*  59 */  <role8, t2, role32, t2, role18>
    /*  60 */  <role8, t3, role32, t5, role18>
    /*  61 */  <role8, t1, role32, t3, role18>
    /*  62 */  <role8, t4, role32, t1, role18>
    /*  63 */  <role8, t5, role32, t4, role18>
    /*  64 */  <role12, t3, role29, t2, role27>
    /*  65 */  <role12, t4, role14, t5, role27>
    /*  66 */  <role12, t5, role14, t1, role27>
    /*  67 */  <role12, t2, role32, t4, role27>
    /*  68 */  <role12, t1, role32, t5, role27>
    /*  69 */  <role12, t5, role32, t2, role27>
    /*  70 */  <TRUE, t3, role32, t3, role27>
    /*  71 */  <role15, t2, NOT ~ role32, t4, role14>
    /*  72 */  <role19, t2, role14, t4, role20>
    /*  73 */  <role19, t3, role14, t5, role20>
    /*  74 */  <role19, t1, role14, t3, role20>
    /*  75 */  <role19, t4, role32, t1, role20>
    /*  76 */  <role19, t2, role32, t5, role20>
    /*  77 */  <role19, t3, role32, t3, role20>
    /*  78 */  <TRUE, t5, role32, t2, role20>
    /*  79 */  <TRUE, t3, role2, t3, role5>
    /*  80 */  <role6, t2, role2, t5, role5>
    /*  81 */  <role6, t5, role2, t1, role5>
    /*  82 */  <TRUE, t3, role22, t4, role5>
    /*  83 */  <role6, t1, role24, t1, role5>
    /*  84 */  <role6, t4, role24, t3, role5>
    /*  85 */  <role6, t4, TRUE, t2, role7>
    /*  86 */  <role6, t2, role24, t4, role5>
    /*  87 */  <role6, t1, role7, t1, role5>
    /*  88 */  <role6, t4, role7, t5, role5>
    /*  89 */  <role6, t5, role28, t5, role5>
    /*  90 */  <role6, t2, role28, t1, role5>
    /*  91 */  <TRUE, t4, role28, t4, role5>
    /*  92 */  <TRUE, t4, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /*  93 */  <role8, t5, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /*  94 */  <role8, t3, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /*  95 */  <role8, t1, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /*  96 */  <role8, t5, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /*  97 */  <role8, t1, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /*  98 */  <TRUE, t5, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /*  99 */  <role8, t4, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 100 */  <role8, t1, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 101 */  <role8, t2, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 102 */  <role8, t4, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 103 */  <TRUE, t5, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 104 */  <role8, t1, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 105 */  <role8, t2, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 106 */  <role8, t4, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 107 */  <TRUE, t1, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 108 */  <TRUE, t5, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 109 */  <TRUE, t5, role22, t1, role16>
    /* 110 */  <role8, t1, role21, t1, role16>
    /* 111 */  <TRUE, t5, role21, t5, role16>
    /* 112 */  <role8, t4, TRUE, t2, role7>
    /* 113 */  <role8, t3, role21, t4, role16>
    /* 114 */  <role8, t2, role6, t4, role16>
    /* 115 */  <role8, t3, role6, t5, role16>
    /* 116 */  <TRUE, t4, role6, t1, role16>
    /* 117 */  <TRUE, t1, role23, t4, role16>
    /* 118 */  <TRUE, t5, role23, t5, role16>
    /* 119 */  <TRUE, t5, role8, t2, role16>
    /* 120 */  <role8, t2, role8, t5, role16>
    /* 121 */  <role8, t2, role12, t5, role16>
    /* 122 */  <role8, t3, role12, t1, role16>
    /* 123 */  <TRUE, t5, role12, t4, role16>
    /* 124 */  <TRUE, t2, role22, t3, role10>
    /* 125 */  <TRUE, t1, role21, t4, role10>
    /* 126 */  <TRUE, t5, role3, t3, role13>
    /* 127 */  <TRUE, t5, role4, t4, role13>
    /* 128 */  <TRUE, t4, role8, t2, role13>
    /* 129 */  <role22, t3, role9, t3, role13>
    /* 130 */  <TRUE, t4, role9, t4, role13>
    /* 131 */  <role22, t2, role9, t5, role13>
    /* 132 */  <role22, t4, role12, t4, role13>
    /* 133 */  <TRUE, t5, role12, t3, role13>
    /* 134 */  <role22, t1, role12, t1, role13>
    /* 135 */  <TRUE, t3, role12, t5, role13>
    /* 136 */  <TRUE, t2, role22, t4, role13>
    /* 137 */  <TRUE, t4, role24, t4, role13>
    /* 138 */  <role22, t1, role24, t1, role13>
    /* 139 */  <TRUE, t1, role23, t3, role13>
    /* 140 */  <TRUE, t5, role23, t1, role13>
    /* 141 */  <role22, t3, role28, t5, role13>
    /* 142 */  <role22, t2, role28, t4, role13>
    /* 143 */  <role22, t5, role28, t3, role13>
    /* 144 */  <role22, t1, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t1, role24>
    /* 145 */  <role24, t3, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t1, role6>
    /* 146 */  <role24, t2, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t2, role6>
    /* 147 */  <role24, t5, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t3, role6>
    /* 148 */  <TRUE, t4, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t5, role6>
    /* 149 */  <role24, t1, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t4, role6>
    /* 150 */  <role6, t1, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t3, role8>
}

/* 
 * Number of Rules       : 32
 * Largest Precondition  : 0
 * Largest Role Schedule : 1
 * Startable Rules       : 32
 * Truly Startable Rules : 6
 */
CanRevoke {
    /*  1 */  <role6, t3, TRUE, t4, role32>
    /*  2 */  <role6, t2, TRUE, t2, role32>
    /*  3 */  <role8, t5, TRUE, t3, role30>
    /*  4 */  <role8, t1, TRUE, t4, role30>
    /*  5 */  <role8, t4, TRUE, t1, role17>
    /*  6 */  <TRUE, t3, TRUE, t2, role17>
    /*  7 */  <TRUE, t5, TRUE, t3, role17>
    /*  8 */  <role6, t2, TRUE, t1, role5>
    /*  9 */  <role6, t3, TRUE, t2, role5>
    /* 10 */  <role7, t3, TRUE, t1, role2>
    /* 11 */  <role7, t5, TRUE, t4, role2>
    /* 12 */  <role7, t4, TRUE, t5, role2>
    /* 13 */  <role8, t1, TRUE, t1, role19>
    /* 14 */  <role8, t3, TRUE, t2, role19>
    /* 15 */  <role8, t4, TRUE, t3, role19>
    /* 16 */  <role8, t2, TRUE, t3, role10>
    /* 17 */  <role8, t5, TRUE, t4, role10>
    /* 18 */  <role8, t1, TRUE, t5, role10>
    /* 19 */  <TRUE, t4, TRUE, t1, role10>
    /* 20 */  <role8, t1, TRUE, t4, role11>
    /* 21 */  <TRUE, t5, TRUE, t2, role11>
    /* 22 */  <role8, t2, TRUE, t3, role25>
    /* 23 */  <role8, t1, TRUE, t4, role25>
    /* 24 */  <role8, t3, TRUE, t1, role25>
    /* 25 */  <TRUE, t4, TRUE, t5, role25>
    /* 26 */  <role22, t2, TRUE, t1, role24>
    /* 27 */  <role22, t4, TRUE, t4, role24>
    /* 28 */  <role24, t5, TRUE, t1, role6>
    /* 29 */  <role24, t1, TRUE, t4, role6>
    /* 30 */  <role24, t2, TRUE, t5, role6>
    /* 31 */  <role6, t5, TRUE, t4, role8>
    /* 32 */  <TRUE, t1, TRUE, t3, role8>
}

/* 
 * Number of Rules       : 29
 * Largest Precondition  : 4
 * Largest Role Schedule : 1
 * Startable Rules       : 6
 * Truly Startable Rules : 0
 */
CanEnable {
    /*  1 */  <role8, t5, TRUE, t3, role30>
    /*  2 */  <role8, t4, TRUE, t1, role17>
    /*  3 */  <role8, t5, TRUE, t2, role16>
    /*  4 */  <role6, t4, TRUE, t1, role14>
    /*  5 */  <role19, t3, TRUE, t5, role20>
    /*  6 */  <role8, t3, TRUE, t1, role15>
    /*  7 */  <TRUE, t1, role6, t4, role10>
    /*  8 */  <TRUE, t2, role22, t3, role10>
    /*  9 */  <role8, t5, role28, t3, role10>
    /* 10 */  <role8, t5, role4, t1, role11>
    /* 11 */  <role8, t1, role22, t2, role11>
    /* 12 */  <role8, t5, role22, t5, role11>
    /* 13 */  <TRUE, t2, role21, t3, role11>
    /* 14 */  <role8, t4, role24, t4, role11>
    /* 15 */  <role8, t2, role23, t2, role11>
    /* 16 */  <role8, t4, role8, t5, role11>
    /* 17 */  <role8, t5, role4, t3, role25>
    /* 18 */  <role8, t5, role22, t5, role25>
    /* 19 */  <role8, t4, role6, t5, role25>
    /* 20 */  <role8, t2, role24, t1, role25>
    /* 21 */  <role22, t3, role6, t2, role13>
    /* 22 */  <role22, t4, role6, t5, role13>
    /* 23 */  <role22, t3, role9, t3, role13>
    /* 24 */  <role22, t1, role12, t1, role13>
    /* 25 */  <TRUE, t3, role12, t5, role13>
    /* 26 */  <role22, t4, role21, t1, role13>
    /* 27 */  <role22, t1, role24, t1, role13>
    /* 28 */  <TRUE, t1, role23, t3, role13>
    /* 29 */  <role22, t1, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t1, role24>
}

/* 
 * Number of Rules       : 13
 * Largest Precondition  : 4
 * Largest Role Schedule : 1
 * Startable Rules       : 5
 * Truly Startable Rules : 1
 */
CanDisable {
    /*  1 */  <role6, t1, TRUE, t2, role14>
    /*  2 */  <TRUE, t2, TRUE, t1, role20>
    /*  3 */  <role19, t5, TRUE, t2, role20>
    /*  4 */  <role12, t2, TRUE, t2, role33>
    /*  5 */  <role7, t3, TRUE, t1, role2>
    /*  6 */  <role8, t5, role7, t5, role10>
    /*  7 */  <role8, t4, role8, t1, role10>
    /*  8 */  <role8, t2, role22, t4, role11>
    /*  9 */  <TRUE, t1, role24, t2, role25>
    /* 10 */  <role8, t2, role16, t1, role26>
    /* 11 */  <TRUE, t5, role3, t3, role13>
    /* 12 */  <role22, t5, role21, t4, role13>
    /* 13 */  <role24, t5, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t3, role6>
}