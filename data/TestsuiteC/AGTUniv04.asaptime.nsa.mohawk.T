/* Generated On        : 2015/02/07 20:24:43.665
 * Generated With      : Mohawk Reverse Converter: ASAPTime NSA
 * Number of Roles     : 33
 * Number of Timeslots : 10
 * Number of Rules     : 491
 * 
 * Roles     : |Size=33| [role1, role2, role3, role4, role5, role6, role7, role8, role9, role10, role11, role12, role13, role14, role15, role16, role17, role18, role19, role20, role21, role22, role23, role24, role25, role27, role28, role29, role30, role31, role32, role33, role34]
 * Timeslots : |Size=10| [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t6, [role31]

Expected: UNKNOWN

/* 
 * Number of Rules       : 303
 * Largest Precondition  : 5
 * Largest Role Schedule : 1
 * Startable Rules       : 63
 * Truly Startable Rules : 11
 */
CanAssign {
    /*   1 */  <role1, t10, role14 & role32, t2, role34>
    /*   2 */  <role1, t1, role14 & role32, t1, role34>
    /*   3 */  <role1, t6, role14 & role32, t3, role34>
    /*   4 */  <TRUE, t7, role14 & role32, t4, role34>
    /*   5 */  <role1, t3, role14 & role32, t8, role34>
    /*   6 */  <role22, t1, TRUE, t8, role23>
    /*   7 */  <TRUE, t6, TRUE, t7, role23>
    /*   8 */  <role22, t3, TRUE, t2, role23>
    /*   9 */  <role22, t2, TRUE, t3, role23>
    /*  10 */  <role22, t8, TRUE, t1, role23>
    /*  11 */  <role22, t6, TRUE, t2, role28>
    /*  12 */  <role22, t7, TRUE, t7, role28>
    /*  13 */  <TRUE, t3, TRUE, t9, role28>
    /*  14 */  <role22, t5, TRUE, t6, role28>
    /*  15 */  <role22, t8, TRUE, t10, role28>
    /*  16 */  <role22, t2, TRUE, t8, role7>
    /*  17 */  <role22, t3, TRUE, t7, role7>
    /*  18 */  <role22, t9, TRUE, t10, role21>
    /*  19 */  <role22, t8, TRUE, t9, role21>
    /*  20 */  <TRUE, t10, TRUE, t2, role3>
    /*  21 */  <role22, t2, TRUE, t8, role3>
    /*  22 */  <role22, t9, TRUE, t3, role3>
    /*  23 */  <role22, t9, TRUE, t7, role4>
    /*  24 */  <TRUE, t10, TRUE, t6, role4>
    /*  25 */  <role22, t5, TRUE, t1, role4>
    /*  26 */  <role22, t7, TRUE, t4, role4>
    /*  27 */  <role22, t8, TRUE, t5, role4>
    /*  28 */  <role22, t1, TRUE, t3, role4>
    /*  29 */  <role22, t6, TRUE, t8, role4>
    /*  30 */  <role22, t2, TRUE, t9, role4>
    /*  31 */  <role22, t3, TRUE, t10, role4>
    /*  32 */  <role22, t8, TRUE, t8, role23>
    /*  33 */  <role22, t5, TRUE, t2, role28>
    /*  34 */  <role22, t9, TRUE, t5, role7>
    /*  35 */  <role22, t6, TRUE, t2, role7>
    /*  36 */  <TRUE, t7, TRUE, t1, role7>
    /*  37 */  <role22, t8, TRUE, t8, role7>
    /*  38 */  <role22, t10, TRUE, t6, role31>
    /*  39 */  <role22, t1, TRUE, t3, role7>
    /*  40 */  <TRUE, t2, TRUE, t4, role7>
    /*  41 */  <role22, t1, TRUE, t9, role21>
    /*  42 */  <role22, t9, TRUE, t3, role21>
    /*  43 */  <TRUE, t2, TRUE, t7, role21>
    /*  44 */  <role22, t3, TRUE, t5, role21>
    /*  45 */  <role22, t4, TRUE, t10, role21>
    /*  46 */  <role22, t5, TRUE, t6, role21>
    /*  47 */  <role22, t6, TRUE, t8, role21>
    /*  48 */  <TRUE, t7, TRUE, t1, role21>
    /*  49 */  <role22, t8, TRUE, t2, role21>
    /*  50 */  <role22, t10, TRUE, t5, role3>
    /*  51 */  <role22, t1, TRUE, t7, role3>
    /*  52 */  <role22, t7, TRUE, t1, role4>
    /*  53 */  <role22, t1, TRUE, t8, role4>
    /*  54 */  <role22, t2, TRUE, t2, role4>
    /*  55 */  <role22, t8, TRUE, t3, role4>
    /*  56 */  <role22, t3, TRUE, t9, role4>
    /*  57 */  <role22, t5, TRUE, t4, role4>
    /*  58 */  <role22, t10, TRUE, t7, role4>
    /*  59 */  <role22, t4, TRUE, t5, role4>
    /*  60 */  <role22, t6, TRUE, t10, role4>
    /*  61 */  <TRUE, t9, TRUE, t6, role4>
    /*  62 */  <role2, t7, NOT ~ role14, t1, role32>
    /*  63 */  <TRUE, t6, NOT ~ role14, t8, role32>
    /*  64 */  <role8, t3, role14, t3, role30>
    /*  65 */  <role8, t5, role14, t9, role30>
    /*  66 */  <role8, t2, role14, t9, role17>
    /*  67 */  <TRUE, t7, role14, t10, role17>
    /*  68 */  <role8, t1, role14, t1, role17>
    /*  69 */  <TRUE, t8, role14, t2, role17>
    /*  70 */  <role8, t6, role17, t10, role16>
    /*  71 */  <role8, t8, role17, t1, role16>
    /*  72 */  <role8, t3, role17, t7, role16>
    /*  73 */  <role8, t7, role17, t2, role16>
    /*  74 */  <role8, t9, role17, t4, role16>
    /*  75 */  <TRUE, t10, role17, t3, role16>
    /*  76 */  <role8, t3, role14, t4, role18>
    /*  77 */  <role8, t4, role14, t1, role18>
    /*  78 */  <TRUE, t5, role14, t8, role18>
    /*  79 */  <TRUE, t6, role14, t6, role18>
    /*  80 */  <TRUE, t7, role14, t9, role18>
    /*  81 */  <role8, t10, role14, t7, role18>
    /*  82 */  <role8, t9, role32, t9, role18>
    /*  83 */  <TRUE, t5, role32, t5, role18>
    /*  84 */  <role8, t3, role32, t8, role18>
    /*  85 */  <role8, t4, role32, t2, role18>
    /*  86 */  <role8, t2, role32, t10, role18>
    /*  87 */  <role8, t6, role32, t1, role18>
    /*  88 */  <role8, t7, role32, t3, role18>
    /*  89 */  <TRUE, t1, role32, t4, role18>
    /*  90 */  <role8, t8, role32, t7, role18>
    /*  91 */  <TRUE, t10, NOT ~ role32, t6, role31>
    /*  92 */  <role12, t10, role29, t5, role27>
    /*  93 */  <role12, t8, role29, t1, role27>
    /*  94 */  <TRUE, t6, role29, t2, role27>
    /*  95 */  <TRUE, t9, role29, t6, role27>
    /*  96 */  <TRUE, t3, role14, t1, role27>
    /*  97 */  <role12, t7, role14, t9, role27>
    /*  98 */  <role12, t5, role14, t7, role27>
    /*  99 */  <role12, t1, role14, t10, role27>
    /* 100 */  <role12, t6, role14, t8, role27>
    /* 101 */  <role12, t10, role14, t3, role27>
    /* 102 */  <role12, t8, role14, t4, role27>
    /* 103 */  <role12, t9, role14, t5, role27>
    /* 104 */  <TRUE, t4, role32, t7, role27>
    /* 105 */  <TRUE, t5, role32, t5, role27>
    /* 106 */  <role12, t1, role32, t1, role27>
    /* 107 */  <TRUE, t2, role32, t6, role27>
    /* 108 */  <role12, t8, role32, t8, role27>
    /* 109 */  <role12, t6, role32, t9, role27>
    /* 110 */  <role12, t7, role32, t2, role27>
    /* 111 */  <role12, t9, role32, t10, role27>
    /* 112 */  <role12, t3, role32, t3, role27>
    /* 113 */  <role12, t10, role32, t4, role27>
    /* 114 */  <role15, t4, NOT ~ role32, t1, role14>
    /* 115 */  <role15, t3, NOT ~ role32, t2, role14>
    /* 116 */  <role15, t5, NOT ~ role32, t5, role14>
    /* 117 */  <role15, t6, NOT ~ role32, t10, role14>
    /* 118 */  <role19, t10, role14, t6, role20>
    /* 119 */  <TRUE, t2, role14, t7, role20>
    /* 120 */  <role19, t1, role14, t8, role20>
    /* 121 */  <role19, t9, role32, t9, role20>
    /* 122 */  <role19, t5, role32, t8, role20>
    /* 123 */  <role19, t8, role32, t10, role20>
    /* 124 */  <role19, t6, role32, t6, role31>
    /* 125 */  <role19, t7, role32, t1, role20>
    /* 126 */  <role19, t10, role32, t4, role20>
    /* 127 */  <role19, t1, role32, t2, role20>
    /* 128 */  <role19, t2, role32, t5, role20>
    /* 129 */  <TRUE, t3, role32, t7, role20>
    /* 130 */  <role19, t4, role32, t3, role20>
    /* 131 */  <role12, t10, role14, t1, role33>
    /* 132 */  <role12, t1, role14, t5, role33>
    /* 133 */  <role12, t8, role14, t9, role33>
    /* 134 */  <TRUE, t6, role14, t2, role33>
    /* 135 */  <role12, t10, role32, t3, role33>
    /* 136 */  <role12, t4, role32, t7, role33>
    /* 137 */  <role6, t4, role2, t4, role5>
    /* 138 */  <role6, t7, role2, t6, role5>
    /* 139 */  <role6, t8, role2, t9, role5>
    /* 140 */  <role6, t3, role2, t2, role5>
    /* 141 */  <role6, t6, role2, t1, role5>
    /* 142 */  <role6, t9, role2, t3, role5>
    /* 143 */  <role6, t10, role2, t5, role5>
    /* 144 */  <role6, t5, role2, t10, role5>
    /* 145 */  <role6, t2, role2, t7, role5>
    /* 146 */  <role6, t10, role22, t1, role5>
    /* 147 */  <role6, t9, role22, t2, role5>
    /* 148 */  <TRUE, t2, role22, t3, role5>
    /* 149 */  <role6, t7, role22, t7, role5>
    /* 150 */  <TRUE, t4, role24, t8, role5>
    /* 151 */  <role6, t9, role24, t1, role5>
    /* 152 */  <role6, t6, role24, t5, role5>
    /* 153 */  <TRUE, t7, role7, t3, role5>
    /* 154 */  <role6, t1, role28, t4, role5>
    /* 155 */  <role6, t8, role28, t8, role5>
    /* 156 */  <TRUE, t10, role28, t5, role5>
    /* 157 */  <role6, t2, role28, t1, role5>
    /* 158 */  <role7, t7, role22 & NOT ~ role15, t7, role2>
    /* 159 */  <role7, t8, role22 & NOT ~ role15, t8, role2>
    /* 160 */  <TRUE, t8, role24 & NOT ~ role15, t7, role2>
    /* 161 */  <role7, t1, role24 & NOT ~ role15, t9, role2>
    /* 162 */  <role7, t9, role24 & NOT ~ role15, t5, role2>
    /* 163 */  <role7, t10, role24 & NOT ~ role15, t10, role2>
    /* 164 */  <role7, t2, role24 & NOT ~ role15, t8, role2>
    /* 165 */  <role7, t3, role7 & NOT ~ role15, t3, role2>
    /* 166 */  <role7, t10, role7 & NOT ~ role15, t10, role2>
    /* 167 */  <TRUE, t4, role7 & NOT ~ role15, t8, role2>
    /* 168 */  <role7, t1, role7 & NOT ~ role15, t1, role2>
    /* 169 */  <role7, t7, role7 & NOT ~ role15, t5, role2>
    /* 170 */  <role7, t2, role7 & NOT ~ role15, t4, role2>
    /* 171 */  <role7, t5, role7 & NOT ~ role15, t9, role2>
    /* 172 */  <role7, t6, role7 & NOT ~ role15, t6, role2>
    /* 173 */  <TRUE, t8, role7 & NOT ~ role15, t7, role2>
    /* 174 */  <role7, t9, role7 & NOT ~ role15, t2, role2>
    /* 175 */  <role7, t2, role28 & NOT ~ role15, t5, role2>
    /* 176 */  <role7, t5, role28 & NOT ~ role15, t2, role2>
    /* 177 */  <TRUE, t1, role28 & NOT ~ role15, t10, role2>
    /* 178 */  <role7, t8, role28 & NOT ~ role15, t1, role2>
    /* 179 */  <role8, t1, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 180 */  <TRUE, t2, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 181 */  <role8, t6, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 182 */  <role8, t3, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 183 */  <role8, t9, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 184 */  <role8, t7, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 185 */  <TRUE, t7, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 186 */  <role8, t3, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 187 */  <role8, t4, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 188 */  <TRUE, t2, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 189 */  <role8, t3, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 190 */  <role8, t4, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 191 */  <role8, t9, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 192 */  <TRUE, t1, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 193 */  <role8, t7, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 194 */  <role8, t5, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 195 */  <role8, t6, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 196 */  <role8, t8, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 197 */  <role8, t10, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 198 */  <TRUE, t3, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 199 */  <role8, t6, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 200 */  <role8, t1, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 201 */  <role8, t5, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 202 */  <TRUE, t4, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 203 */  <TRUE, t8, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 204 */  <role8, t4, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 205 */  <TRUE, t10, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 206 */  <role8, t3, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 207 */  <role8, t2, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 208 */  <role8, t7, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 209 */  <role8, t5, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 210 */  <role8, t1, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 211 */  <role8, t8, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 212 */  <role8, t4, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 213 */  <role8, t3, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 214 */  <role8, t2, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 215 */  <TRUE, t10, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 216 */  <TRUE, t6, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 217 */  <role8, t1, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 218 */  <role8, t5, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 219 */  <TRUE, t7, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 220 */  <role8, t8, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 221 */  <role8, t9, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 222 */  <role8, t5, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 223 */  <role8, t9, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 224 */  <role8, t2, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 225 */  <TRUE, t6, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 226 */  <TRUE, t7, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 227 */  <TRUE, t8, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 228 */  <role8, t3, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 229 */  <role8, t4, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 230 */  <role8, t6, role3, t10, role16>
    /* 231 */  <role8, t8, role4, t8, role16>
    /* 232 */  <role8, t2, role4, t5, role16>
    /* 233 */  <TRUE, t10, role4, t9, role16>
    /* 234 */  <role8, t3, role4, t10, role16>
    /* 235 */  <role8, t5, role4, t2, role16>
    /* 236 */  <role8, t4, role4, t1, role16>
    /* 237 */  <TRUE, t7, role4, t6, role16>
    /* 238 */  <role8, t8, role22, t10, role16>
    /* 239 */  <role8, t1, role22, t3, role16>
    /* 240 */  <role8, t3, role22, t1, role16>
    /* 241 */  <role8, t2, role22, t5, role16>
    /* 242 */  <role8, t4, role22, t4, role16>
    /* 243 */  <role8, t5, role22, t6, role16>
    /* 244 */  <role8, t1, role21, t9, role16>
    /* 245 */  <TRUE, t3, role21, t10, role16>
    /* 246 */  <role8, t7, role21, t8, role16>
    /* 247 */  <TRUE, t2, role21, t4, role16>
    /* 248 */  <role8, t5, role6, t2, role16>
    /* 249 */  <role8, t1, role6, t4, role16>
    /* 250 */  <role8, t9, role6, t6, role16>
    /* 251 */  <TRUE, t2, role6, t7, role16>
    /* 252 */  <TRUE, t6, role6, t3, role16>
    /* 253 */  <role8, t10, role6, t8, role16>
    /* 254 */  <role8, t3, role6, t5, role16>
    /* 255 */  <role8, t4, role6, t9, role16>
    /* 256 */  <role8, t10, role24, t10, role16>
    /* 257 */  <TRUE, t8, role23, t2, role16>
    /* 258 */  <TRUE, t6, role23, t10, role16>
    /* 259 */  <role8, t10, role23, t1, role16>
    /* 260 */  <role8, t1, role23, t8, role16>
    /* 261 */  <role8, t2, role23, t3, role16>
    /* 262 */  <role8, t7, role8, t2, role16>
    /* 263 */  <role8, t1, role8, t10, role16>
    /* 264 */  <role8, t8, role21, t10, role10>
    /* 265 */  <TRUE, t9, role21, t3, role10>
    /* 266 */  <role8, t4, role24, t9, role10>
    /* 267 */  <role8, t7, role23, t10, role10>
    /* 268 */  <role8, t4, role23, t1, role10>
    /* 269 */  <role8, t1, role23, t6, role10>
    /* 270 */  <role8, t1, role28, t3, role10>
    /* 271 */  <TRUE, t2, role28, t5, role10>
    /* 272 */  <role8, t8, role3, t6, role11>
    /* 273 */  <role8, t1, role3, t7, role11>
    /* 274 */  <role8, t2, role3, t10, role11>
    /* 275 */  <role8, t5, role3, t8, role11>
    /* 276 */  <role8, t3, role3, t5, role11>
    /* 277 */  <role8, t4, role3, t1, role11>
    /* 278 */  <TRUE, t6, role3, t9, role11>
    /* 279 */  <role8, t10, role3, t2, role11>
    /* 280 */  <role8, t1, role4, t3, role11>
    /* 281 */  <role8, t8, role4, t1, role11>
    /* 282 */  <role8, t2, role4, t10, role11>
    /* 283 */  <role8, t10, role4, t2, role11>
    /* 284 */  <role8, t6, role4, t9, role11>
    /* 285 */  <role8, t3, role4, t4, role11>
    /* 286 */  <TRUE, t9, role4, t6, role11>
    /* 287 */  <role8, t4, role4, t7, role11>
    /* 288 */  <role8, t5, role22, t9, role11>
    /* 289 */  <TRUE, t2, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t1, role24>
    /* 290 */  <TRUE, t1, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t8, role6>
    /* 291 */  <role24, t6, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t9, role6>
    /* 292 */  <role24, t9, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t10, role6>
    /* 293 */  <role24, t5, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t1, role6>
    /* 294 */  <role24, t3, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t5, role6>
    /* 295 */  <role24, t2, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t2, role6>
    /* 296 */  <role24, t7, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t3, role6>
    /* 297 */  <TRUE, t6, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t2, role8>
    /* 298 */  <role6, t9, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t3, role8>
    /* 299 */  <role6, t8, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t10, role8>
    /* 300 */  <role6, t10, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t4, role8>
    /* 301 */  <role6, t4, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t6, role8>
    /* 302 */  <role6, t1, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t7, role8>
    /* 303 */  <role6, t2, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t8, role8>
}

/* 
 * Number of Rules       : 43
 * Largest Precondition  : 0
 * Largest Role Schedule : 1
 * Startable Rules       : 43
 * Truly Startable Rules : 8
 */
CanRevoke {
    /*  1 */  <role6, t6, TRUE, t3, role32>
    /*  2 */  <role6, t8, TRUE, t2, role32>
    /*  3 */  <role6, t2, TRUE, t7, role32>
    /*  4 */  <role6, t7, TRUE, t8, role32>
    /*  5 */  <role6, t9, TRUE, t10, role32>
    /*  6 */  <TRUE, t3, TRUE, t9, role32>
    /*  7 */  <role6, t4, TRUE, t4, role32>
    /*  8 */  <role8, t10, TRUE, t4, role30>
    /*  9 */  <TRUE, t7, TRUE, t5, role30>
    /* 10 */  <role8, t10, TRUE, t2, role17>
    /* 11 */  <role8, t8, TRUE, t6, role17>
    /* 12 */  <role8, t6, TRUE, t1, role16>
    /* 13 */  <role8, t5, TRUE, t6, role16>
    /* 14 */  <role8, t4, TRUE, t4, role16>
    /* 15 */  <TRUE, t7, TRUE, t10, role16>
    /* 16 */  <role8, t8, TRUE, t5, role16>
    /* 17 */  <role8, t2, TRUE, t2, role16>
    /* 18 */  <role8, t3, TRUE, t7, role16>
    /* 19 */  <TRUE, t9, TRUE, t8, role16>
    /* 20 */  <role8, t7, TRUE, t9, role18>
    /* 21 */  <TRUE, t8, TRUE, t2, role18>
    /* 22 */  <role8, t1, TRUE, t3, role18>
    /* 23 */  <TRUE, t5, TRUE, t4, role14>
    /* 24 */  <TRUE, t3, TRUE, t6, role33>
    /* 25 */  <role6, t7, TRUE, t9, role5>
    /* 26 */  <role6, t8, TRUE, t6, role5>
    /* 27 */  <role6, t3, TRUE, t3, role5>
    /* 28 */  <role7, t10, TRUE, t6, role2>
    /* 29 */  <role7, t8, TRUE, t2, role2>
    /* 30 */  <role22, t6, TRUE, t1, role13>
    /* 31 */  <TRUE, t7, TRUE, t10, role13>
    /* 32 */  <role22, t2, TRUE, t4, role24>
    /* 33 */  <role22, t3, TRUE, t8, role24>
    /* 34 */  <role22, t6, TRUE, t5, role24>
    /* 35 */  <role22, t5, TRUE, t9, role24>
    /* 36 */  <role22, t7, TRUE, t6, role24>
    /* 37 */  <role22, t8, TRUE, t7, role24>
    /* 38 */  <role24, t3, TRUE, t6, role6>
    /* 39 */  <role24, t4, TRUE, t7, role6>
    /* 40 */  <role24, t6, TRUE, t5, role6>
    /* 41 */  <role6, t9, TRUE, t6, role8>
    /* 42 */  <role6, t5, TRUE, t1, role8>
    /* 43 */  <role6, t1, TRUE, t7, role8>
}

/* 
 * Number of Rules       : 96
 * Largest Precondition  : 5
 * Largest Role Schedule : 1
 * Startable Rules       : 10
 * Truly Startable Rules : 1
 */
CanEnable {
    /*  1 */  <role6, t8, TRUE, t2, role32>
    /*  2 */  <role8, t10, TRUE, t2, role17>
    /*  3 */  <role8, t1, TRUE, t3, role17>
    /*  4 */  <role8, t8, TRUE, t6, role17>
    /*  5 */  <role8, t6, TRUE, t1, role16>
    /*  6 */  <role8, t5, TRUE, t6, role16>
    /*  7 */  <role8, t4, TRUE, t4, role16>
    /*  8 */  <TRUE, t9, TRUE, t8, role16>
    /*  9 */  <role6, t4, TRUE, t10, role14>
    /* 10 */  <role19, t5, TRUE, t3, role20>
    /* 11 */  <TRUE, t7, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 12 */  <role8, t5, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 13 */  <TRUE, t8, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 14 */  <role8, t2, role4, t5, role16>
    /* 15 */  <role8, t3, role4, t10, role16>
    /* 16 */  <role8, t4, role4, t1, role16>
    /* 17 */  <role8, t1, role22, t3, role16>
    /* 18 */  <role8, t2, role22, t5, role16>
    /* 19 */  <role8, t4, role22, t4, role16>
    /* 20 */  <role8, t1, role6, t4, role16>
    /* 21 */  <role8, t10, role6, t8, role16>
    /* 22 */  <TRUE, t2, role8, t1, role16>
    /* 23 */  <TRUE, t5, role12, t1, role16>
    /* 24 */  <role8, t3, role12, t2, role16>
    /* 25 */  <role8, t6, role12, t9, role16>
    /* 26 */  <role8, t5, role3, t10, role19>
    /* 27 */  <role8, t3, role4, t4, role19>
    /* 28 */  <role8, t8, role4, t7, role19>
    /* 29 */  <role8, t4, role22, t6, role19>
    /* 30 */  <role8, t7, role22, t10, role19>
    /* 31 */  <role8, t1, role21, t3, role19>
    /* 32 */  <role8, t7, role21, t2, role19>
    /* 33 */  <role8, t8, role23, t5, role19>
    /* 34 */  <role8, t9, role8, t10, role19>
    /* 35 */  <role8, t8, role12, t3, role19>
    /* 36 */  <role8, t4, role12, t4, role19>
    /* 37 */  <role8, t10, role2, t8, role10>
    /* 38 */  <TRUE, t6, role2, t3, role10>
    /* 39 */  <TRUE, t1, role3, t4, role10>
    /* 40 */  <role8, t10, role4, t8, role10>
    /* 41 */  <role8, t4, role6, t6, role10>
    /* 42 */  <TRUE, t6, role6, t9, role10>
    /* 43 */  <TRUE, t5, role7, t6, role10>
    /* 44 */  <role8, t6, role7, t3, role10>
    /* 45 */  <role8, t3, role9, t8, role10>
    /* 46 */  <role8, t5, role9, t5, role10>
    /* 47 */  <role8, t10, role12, t7, role10>
    /* 48 */  <role8, t5, role22, t6, role10>
    /* 49 */  <role8, t6, role22, t3, role10>
    /* 50 */  <TRUE, t9, role21, t3, role10>
    /* 51 */  <role8, t1, role3, t7, role11>
    /* 52 */  <role8, t5, role3, t8, role11>
    /* 53 */  <role8, t3, role3, t5, role11>
    /* 54 */  <role8, t4, role3, t1, role11>
    /* 55 */  <role8, t10, role4, t2, role11>
    /* 56 */  <role8, t3, role4, t4, role11>
    /* 57 */  <TRUE, t9, role4, t6, role11>
    /* 58 */  <role8, t6, role22, t2, role11>
    /* 59 */  <role8, t6, role21, t5, role11>
    /* 60 */  <TRUE, t1, role6, t6, role11>
    /* 61 */  <role8, t2, role6, t3, role11>
    /* 62 */  <TRUE, t1, role8, t8, role11>
    /* 63 */  <role8, t9, role8, t3, role11>
    /* 64 */  <role8, t8, role8, t10, role11>
    /* 65 */  <role8, t3, role8, t1, role11>
    /* 66 */  <TRUE, t9, role12, t10, role11>
    /* 67 */  <role8, t3, role22, t7, role25>
    /* 68 */  <role8, t5, role22, t3, role25>
    /* 69 */  <role8, t4, role6, t2, role25>
    /* 70 */  <role8, t10, role6, t8, role25>
    /* 71 */  <role8, t4, role24, t8, role25>
    /* 72 */  <role8, t2, role24, t10, role25>
    /* 73 */  <role8, t1, role23, t6, role25>
    /* 74 */  <role8, t6, role23, t1, role25>
    /* 75 */  <role8, t3, role8, t9, role25>
    /* 76 */  <role8, t6, role12, t4, role25>
    /* 77 */  <role8, t2, role12, t5, role25>
    /* 78 */  <TRUE, t3, role12, t9, role25>
    /* 79 */  <role22, t2, role2, t1, role13>
    /* 80 */  <role22, t2, role3, t7, role13>
    /* 81 */  <TRUE, t3, role3, t9, role13>
    /* 82 */  <role22, t4, role4, t1, role13>
    /* 83 */  <role22, t10, role7, t4, role13>
    /* 84 */  <TRUE, t5, role7, t2, role13>
    /* 85 */  <role22, t8, role8, t5, role13>
    /* 86 */  <TRUE, t7, role8, t7, role13>
    /* 87 */  <TRUE, t9, role8, t10, role13>
    /* 88 */  <role22, t8, role9, t1, role13>
    /* 89 */  <TRUE, t10, role12, t2, role13>
    /* 90 */  <role22, t7, role12, t9, role13>
    /* 91 */  <role22, t8, role12, t4, role13>
    /* 92 */  <role22, t5, role23, t4, role13>
    /* 93 */  <role22, t4, role28, t8, role13>
    /* 94 */  <role24, t7, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t3, role6>
    /* 95 */  <role6, t4, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t6, role8>
    /* 96 */  <role6, t2, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t8, role8>
}

/* 
 * Number of Rules       : 49
 * Largest Precondition  : 5
 * Largest Role Schedule : 1
 * Startable Rules       : 3
 * Truly Startable Rules : 2
 */
CanDisable {
    /*  1 */  <TRUE, t7, TRUE, t5, role30>
    /*  2 */  <role8, t2, TRUE, t2, role16>
    /*  3 */  <TRUE, t3, TRUE, t9, role14>
    /*  4 */  <TRUE, t6, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /*  5 */  <role8, t3, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /*  6 */  <role8, t4, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /*  7 */  <role8, t8, role4, t8, role16>
    /*  8 */  <role8, t5, role4, t2, role16>
    /*  9 */  <role8, t7, role21, t8, role16>
    /* 10 */  <TRUE, t2, role6, t7, role16>
    /* 11 */  <role8, t1, role23, t8, role16>
    /* 12 */  <role8, t2, role23, t3, role16>
    /* 13 */  <role8, t3, role8, t6, role16>
    /* 14 */  <role8, t4, role3, t6, role19>
    /* 15 */  <TRUE, t6, role22, t1, role19>
    /* 16 */  <role8, t6, role21, t5, role19>
    /* 17 */  <TRUE, t10, role24, t1, role19>
    /* 18 */  <role8, t4, role8, t4, role19>
    /* 19 */  <role8, t8, role8, t8, role19>
    /* 20 */  <role8, t8, role2, t5, role10>
    /* 21 */  <role8, t9, role4, t2, role10>
    /* 22 */  <role8, t1, role6, t7, role10>
    /* 23 */  <TRUE, t1, role9, t4, role10>
    /* 24 */  <role8, t8, role9, t7, role10>
    /* 25 */  <TRUE, t3, role12, t5, role10>
    /* 26 */  <TRUE, t8, role22, t9, role10>
    /* 27 */  <role8, t4, role24, t9, role10>
    /* 28 */  <role8, t10, role3, t2, role11>
    /* 29 */  <TRUE, t2, role21, t7, role11>
    /* 30 */  <role8, t10, role8, t7, role11>
    /* 31 */  <role8, t7, role3, t1, role25>
    /* 32 */  <role8, t7, role22, t6, role25>
    /* 33 */  <role8, t2, role21, t8, role25>
    /* 34 */  <TRUE, t9, role6, t6, role25>
    /* 35 */  <TRUE, t1, role24, t1, role25>
    /* 36 */  <role8, t5, role12, t10, role25>
    /* 37 */  <role22, t5, role2, t5, role13>
    /* 38 */  <role22, t4, role6, t2, role13>
    /* 39 */  <role22, t7, role6, t3, role13>
    /* 40 */  <role22, t5, role8, t8, role13>
    /* 41 */  <role22, t6, role9, t2, role13>
    /* 42 */  <role22, t8, role22, t7, role13>
    /* 43 */  <TRUE, t8, role23, t8, role13>
    /* 44 */  <role22, t9, role23, t5, role13>
    /* 45 */  <TRUE, t2, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t1, role24>
    /* 46 */  <role24, t9, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t10, role6>
    /* 47 */  <role24, t2, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t2, role6>
    /* 48 */  <TRUE, t6, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t2, role8>
    /* 49 */  <role6, t1, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t7, role8>
}