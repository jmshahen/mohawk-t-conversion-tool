/* Generated On        : 2015/02/07 20:24:43.651
 * Generated With      : Mohawk Reverse Converter: ASAPTime NSA
 * Number of Roles     : 31
 * Number of Timeslots : 11
 * Number of Rules     : 483
 * 
 * Roles     : |Size=31| [role1, role2, role3, role4, role5, role6, role7, role8, role10, role11, role12, role14, role15, role16, role17, role18, role19, role20, role21, role22, role23, role24, role25, role26, role27, role28, role29, role30, role32, role33, role34]
 * Timeslots : |Size=11| [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t16]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t9, [role26]

Expected: UNKNOWN

/* 
 * Number of Rules       : 331
 * Largest Precondition  : 5
 * Largest Role Schedule : 1
 * Startable Rules       : 82
 * Truly Startable Rules : 15
 */
CanAssign {
    /*   1 */  <role1, t8, role6 & role8, t9, role34>
    /*   2 */  <TRUE, t5, role6 & role8, t7, role34>
    /*   3 */  <role1, t1, role6 & role8, t10, role34>
    /*   4 */  <role1, t10, role6 & role8, t8, role34>
    /*   5 */  <role1, t7, role6 & role8, t4, role34>
    /*   6 */  <role1, t9, role6 & role8, t1, role34>
    /*   7 */  <TRUE, t2, role6 & role8, t2, role34>
    /*   8 */  <role1, t3, role6 & role8, t3, role34>
    /*   9 */  <role22, t9, TRUE, t1, role23>
    /*  10 */  <TRUE, t1, TRUE, t6, role23>
    /*  11 */  <role22, t16, role7, t9, role26>
    /*  12 */  <role22, t3, TRUE, t3, role23>
    /*  13 */  <role22, t5, TRUE, t5, role23>
    /*  14 */  <role22, t8, TRUE, t8, role23>
    /*  15 */  <role22, t2, TRUE, t9, role23>
    /*  16 */  <role22, t6, TRUE, t7, role23>
    /*  17 */  <role22, t4, TRUE, t4, role23>
    /*  18 */  <role22, t7, TRUE, t10, role23>
    /*  19 */  <role22, t10, TRUE, t4, role28>
    /*  20 */  <TRUE, t5, TRUE, t3, role7>
    /*  21 */  <role22, t4, TRUE, t5, role7>
    /*  22 */  <role22, t1, TRUE, t10, role7>
    /*  23 */  <role22, t3, TRUE, t9, role7>
    /*  24 */  <role22, t2, TRUE, t6, role7>
    /*  25 */  <TRUE, t10, TRUE, t1, role7>
    /*  26 */  <role22, t6, TRUE, t2, role7>
    /*  27 */  <role22, t9, TRUE, t4, role7>
    /*  28 */  <TRUE, t7, TRUE, t7, role7>
    /*  29 */  <role22, t3, TRUE, t1, role21>
    /*  30 */  <role22, t2, TRUE, t10, role21>
    /*  31 */  <TRUE, t4, TRUE, t6, role21>
    /*  32 */  <role22, t6, TRUE, t3, role3>
    /*  33 */  <TRUE, t8, TRUE, t4, role3>
    /*  34 */  <role22, t7, TRUE, t5, role3>
    /*  35 */  <role22, t10, TRUE, t7, role3>
    /*  36 */  <role22, t1, TRUE, t8, role3>
    /*  37 */  <TRUE, t4, TRUE, t9, role3>
    /*  38 */  <role22, t5, TRUE, t2, role3>
    /*  39 */  <role22, t4, TRUE, t3, role4>
    /*  40 */  <TRUE, t2, TRUE, t9, role23>
    /*  41 */  <role22, t3, TRUE, t10, role23>
    /*  42 */  <role22, t4, TRUE, t8, role23>
    /*  43 */  <role22, t5, TRUE, t5, role23>
    /*  44 */  <role22, t8, TRUE, t6, role23>
    /*  45 */  <role22, t3, TRUE, t2, role28>
    /*  46 */  <role22, t8, TRUE, t3, role28>
    /*  47 */  <role22, t4, TRUE, t9, role28>
    /*  48 */  <role22, t6, TRUE, t1, role28>
    /*  49 */  <role22, t5, TRUE, t7, role28>
    /*  50 */  <TRUE, t10, TRUE, t4, role28>
    /*  51 */  <role22, t7, TRUE, t6, role28>
    /*  52 */  <role22, t9, TRUE, t10, role28>
    /*  53 */  <role22, t1, TRUE, t8, role28>
    /*  54 */  <role22, t2, TRUE, t5, role28>
    /*  55 */  <TRUE, t3, TRUE, t2, role7>
    /*  56 */  <role22, t9, TRUE, t8, role7>
    /*  57 */  <role22, t4, TRUE, t9, role7>
    /*  58 */  <role22, t8, TRUE, t1, role7>
    /*  59 */  <role22, t7, TRUE, t4, role7>
    /*  60 */  <role22, t5, TRUE, t10, role7>
    /*  61 */  <role22, t5, TRUE, t1, role21>
    /*  62 */  <role22, t8, TRUE, t10, role21>
    /*  63 */  <TRUE, t7, TRUE, t9, role21>
    /*  64 */  <role22, t9, TRUE, t6, role21>
    /*  65 */  <TRUE, t3, TRUE, t8, role21>
    /*  66 */  <role22, t6, TRUE, t3, role21>
    /*  67 */  <role22, t2, TRUE, t7, role21>
    /*  68 */  <role22, t4, TRUE, t4, role21>
    /*  69 */  <role22, t10, TRUE, t5, role21>
    /*  70 */  <role22, t1, TRUE, t2, role21>
    /*  71 */  <role22, t4, TRUE, t2, role3>
    /*  72 */  <role22, t10, TRUE, t7, role3>
    /*  73 */  <role22, t2, TRUE, t1, role3>
    /*  74 */  <role22, t1, TRUE, t5, role3>
    /*  75 */  <TRUE, t6, TRUE, t3, role3>
    /*  76 */  <role22, t3, TRUE, t4, role3>
    /*  77 */  <role22, t7, TRUE, t8, role3>
    /*  78 */  <role22, t5, TRUE, t1, role4>
    /*  79 */  <role22, t3, TRUE, t7, role4>
    /*  80 */  <role22, t1, TRUE, t3, role4>
    /*  81 */  <TRUE, t2, NOT ~ role14, t1, role32>
    /*  82 */  <role2, t6, NOT ~ role14, t2, role32>
    /*  83 */  <role2, t7, NOT ~ role14, t3, role32>
    /*  84 */  <TRUE, t5, NOT ~ role14, t4, role32>
    /*  85 */  <role8, t2, role14, t1, role30>
    /*  86 */  <role8, t3, role14, t4, role30>
    /*  87 */  <role8, t3, role14, t10, role17>
    /*  88 */  <TRUE, t9, role14, t3, role17>
    /*  89 */  <TRUE, t8, role14, t2, role17>
    /*  90 */  <role8, t10, role14, t7, role17>
    /*  91 */  <role8, t1, role14, t4, role17>
    /*  92 */  <role8, t2, role17, t6, role16>
    /*  93 */  <role8, t9, role17, t10, role16>
    /*  94 */  <role8, t8, role17, t3, role16>
    /*  95 */  <role8, t4, role14, t8, role18>
    /*  96 */  <role8, t2, role14, t9, role18>
    /*  97 */  <role8, t8, role32, t1, role18>
    /*  98 */  <role8, t9, role32, t4, role18>
    /*  99 */  <role8, t10, role32, t8, role18>
    /* 100 */  <role8, t2, role32, t6, role18>
    /* 101 */  <role8, t1, role32, t3, role18>
    /* 102 */  <role8, t3, role32, t5, role18>
    /* 103 */  <role12, t2, role29, t1, role27>
    /* 104 */  <role12, t3, role29, t6, role27>
    /* 105 */  <role12, t4, role29, t7, role27>
    /* 106 */  <role12, t5, role29, t8, role27>
    /* 107 */  <role12, t6, role29, t9, role27>
    /* 108 */  <role12, t10, role29, t10, role27>
    /* 109 */  <TRUE, t7, role29, t2, role27>
    /* 110 */  <role12, t8, role29, t3, role27>
    /* 111 */  <TRUE, t9, role29, t4, role27>
    /* 112 */  <TRUE, t1, role29, t5, role27>
    /* 113 */  <role12, t8, role14, t4, role27>
    /* 114 */  <role12, t10, role14, t7, role27>
    /* 115 */  <TRUE, t3, role14, t5, role27>
    /* 116 */  <role12, t1, role14, t6, role27>
    /* 117 */  <role12, t9, role14, t10, role27>
    /* 118 */  <role12, t4, role14, t9, role27>
    /* 119 */  <role12, t5, role14, t1, role27>
    /* 120 */  <role12, t2, role14, t2, role27>
    /* 121 */  <role12, t6, role14, t8, role27>
    /* 122 */  <role12, t7, role14, t3, role27>
    /* 123 */  <role12, t1, role32, t6, role27>
    /* 124 */  <role12, t9, role32, t7, role27>
    /* 125 */  <role12, t3, role32, t4, role27>
    /* 126 */  <role12, t8, role32, t5, role27>
    /* 127 */  <TRUE, t4, role32, t9, role27>
    /* 128 */  <role12, t10, role32, t8, role27>
    /* 129 */  <TRUE, t2, role32, t10, role27>
    /* 130 */  <role12, t5, role32, t1, role27>
    /* 131 */  <TRUE, t6, role32, t2, role27>
    /* 132 */  <role15, t1, NOT ~ role32, t2, role14>
    /* 133 */  <role15, t9, NOT ~ role32, t3, role14>
    /* 134 */  <role15, t6, NOT ~ role32, t9, role14>
    /* 135 */  <role15, t2, NOT ~ role32, t4, role14>
    /* 136 */  <role15, t7, NOT ~ role32, t5, role14>
    /* 137 */  <role15, t8, NOT ~ role32, t1, role14>
    /* 138 */  <role15, t4, NOT ~ role32, t6, role14>
    /* 139 */  <role19, t6, role14, t7, role20>
    /* 140 */  <role19, t9, role14, t2, role20>
    /* 141 */  <role19, t1, role14, t9, role20>
    /* 142 */  <role19, t7, role14, t3, role20>
    /* 143 */  <role19, t5, role14, t8, role20>
    /* 144 */  <role19, t8, role14, t5, role20>
    /* 145 */  <role19, t10, role14, t10, role20>
    /* 146 */  <role19, t2, role14, t1, role20>
    /* 147 */  <role19, t3, role14, t4, role20>
    /* 148 */  <role19, t4, role14, t6, role20>
    /* 149 */  <TRUE, t3, role32, t7, role20>
    /* 150 */  <TRUE, t7, role14, t1, role33>
    /* 151 */  <role12, t5, role14, t8, role33>
    /* 152 */  <role12, t6, role14, t7, role33>
    /* 153 */  <role12, t4, role32, t6, role33>
    /* 154 */  <role12, t2, role32, t2, role33>
    /* 155 */  <role6, t8, role2, t6, role5>
    /* 156 */  <role6, t3, role2, t10, role5>
    /* 157 */  <role6, t10, role2, t7, role5>
    /* 158 */  <role6, t5, role2, t3, role5>
    /* 159 */  <role6, t1, role2, t2, role5>
    /* 160 */  <role6, t4, role2, t8, role5>
    /* 161 */  <role6, t2, role2, t1, role5>
    /* 162 */  <role6, t7, role2, t4, role5>
    /* 163 */  <role6, t8, role22, t1, role5>
    /* 164 */  <role6, t6, role22, t8, role5>
    /* 165 */  <role6, t9, role22, t7, role5>
    /* 166 */  <role6, t6, role24, t9, role5>
    /* 167 */  <role6, t5, role24, t3, role5>
    /* 168 */  <TRUE, t3, role24, t10, role5>
    /* 169 */  <role6, t7, role24, t6, role5>
    /* 170 */  <role6, t2, role7, t3, role5>
    /* 171 */  <role6, t1, role7, t7, role5>
    /* 172 */  <role6, t4, role7, t4, role5>
    /* 173 */  <role6, t7, role28, t5, role5>
    /* 174 */  <TRUE, t3, role22 & NOT ~ role15, t10, role2>
    /* 175 */  <role7, t2, role22 & NOT ~ role15, t4, role2>
    /* 176 */  <role7, t5, role24 & NOT ~ role15, t3, role2>
    /* 177 */  <role7, t7, role24 & NOT ~ role15, t6, role2>
    /* 178 */  <role7, t10, role24 & NOT ~ role15, t5, role2>
    /* 179 */  <role7, t4, role24 & NOT ~ role15, t8, role2>
    /* 180 */  <role7, t6, role24 & NOT ~ role15, t9, role2>
    /* 181 */  <role7, t1, role24 & NOT ~ role15, t4, role2>
    /* 182 */  <TRUE, t8, role24 & NOT ~ role15, t10, role2>
    /* 183 */  <role7, t9, role24 & NOT ~ role15, t1, role2>
    /* 184 */  <role7, t2, role24 & NOT ~ role15, t2, role2>
    /* 185 */  <role7, t3, role24 & NOT ~ role15, t7, role2>
    /* 186 */  <role7, t8, role7 & NOT ~ role15, t7, role2>
    /* 187 */  <role7, t9, role28 & NOT ~ role15, t2, role2>
    /* 188 */  <role8, t7, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 189 */  <role8, t4, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 190 */  <role8, t2, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 191 */  <TRUE, t3, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 192 */  <role8, t1, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 193 */  <role8, t4, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 194 */  <role8, t2, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 195 */  <role8, t5, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 196 */  <TRUE, t9, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 197 */  <role8, t10, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 198 */  <role8, t5, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 199 */  <role8, t8, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 200 */  <TRUE, t3, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 201 */  <TRUE, t9, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 202 */  <role8, t10, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 203 */  <role8, t1, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 204 */  <TRUE, t5, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 205 */  <role8, t6, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 206 */  <role8, t9, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 207 */  <role8, t3, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 208 */  <role8, t7, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 209 */  <role8, t2, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 210 */  <TRUE, t4, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 211 */  <role8, t10, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 212 */  <role8, t7, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 213 */  <role8, t4, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 214 */  <role8, t9, role3, t5, role16>
    /* 215 */  <role8, t10, role3, t8, role16>
    /* 216 */  <TRUE, t2, role3, t4, role16>
    /* 217 */  <role8, t1, role3, t6, role16>
    /* 218 */  <role8, t4, role3, t7, role16>
    /* 219 */  <TRUE, t1, role4, t5, role16>
    /* 220 */  <TRUE, t4, role22, t3, role16>
    /* 221 */  <role8, t3, role22, t4, role16>
    /* 222 */  <role8, t5, role22, t6, role16>
    /* 223 */  <role8, t6, role22, t5, role16>
    /* 224 */  <role8, t9, role22, t7, role16>
    /* 225 */  <role8, t9, role21, t5, role16>
    /* 226 */  <role8, t4, role21, t7, role16>
    /* 227 */  <role8, t10, role21, t9, role16>
    /* 228 */  <role8, t7, role21, t10, role16>
    /* 229 */  <role8, t5, role21, t8, role16>
    /* 230 */  <role8, t6, role21, t1, role16>
    /* 231 */  <role8, t10, role6, t4, role16>
    /* 232 */  <role8, t1, role6, t5, role16>
    /* 233 */  <TRUE, t5, role6, t7, role16>
    /* 234 */  <role8, t2, role6, t1, role16>
    /* 235 */  <TRUE, t1, role24, t10, role16>
    /* 236 */  <role8, t8, role24, t9, role16>
    /* 237 */  <role8, t9, role24, t2, role16>
    /* 238 */  <TRUE, t2, role24, t8, role16>
    /* 239 */  <role8, t10, role24, t1, role16>
    /* 240 */  <role8, t6, role24, t4, role16>
    /* 241 */  <TRUE, t3, role24, t3, role16>
    /* 242 */  <role8, t4, role24, t5, role16>
    /* 243 */  <role8, t8, role23, t3, role16>
    /* 244 */  <role8, t6, role23, t8, role16>
    /* 245 */  <role8, t3, role23, t6, role16>
    /* 246 */  <role8, t4, role8, t10, role16>
    /* 247 */  <role8, t8, role8, t3, role16>
    /* 248 */  <role8, t9, role8, t1, role16>
    /* 249 */  <role8, t1, role12, t10, role16>
    /* 250 */  <role8, t5, role12, t9, role16>
    /* 251 */  <role8, t4, role12, t1, role16>
    /* 252 */  <TRUE, t7, role12, t4, role16>
    /* 253 */  <role8, t3, role12, t2, role16>
    /* 254 */  <TRUE, t2, role12, t3, role16>
    /* 255 */  <role8, t6, role12, t5, role16>
    /* 256 */  <role8, t2, role3, t10, role19>
    /* 257 */  <role8, t4, role3, t9, role19>
    /* 258 */  <TRUE, t5, role3, t8, role19>
    /* 259 */  <role8, t1, role3, t1, role19>
    /* 260 */  <role8, t3, role3, t2, role19>
    /* 261 */  <role8, t6, role3, t7, role19>
    /* 262 */  <role8, t8, role3, t3, role19>
    /* 263 */  <role8, t7, role3, t5, role19>
    /* 264 */  <role8, t10, role3, t4, role19>
    /* 265 */  <TRUE, t9, role3, t6, role19>
    /* 266 */  <role8, t1, role4, t8, role19>
    /* 267 */  <TRUE, t5, role4, t6, role19>
    /* 268 */  <role8, t1, role22, t6, role19>
    /* 269 */  <role8, t5, role22, t10, role19>
    /* 270 */  <TRUE, t4, role21, t8, role19>
    /* 271 */  <role8, t7, role21, t5, role19>
    /* 272 */  <role8, t5, role21, t3, role19>
    /* 273 */  <TRUE, t3, role21, t2, role19>
    /* 274 */  <role8, t8, role21, t6, role19>
    /* 275 */  <role8, t9, role21, t9, role19>
    /* 276 */  <role8, t10, role21, t10, role19>
    /* 277 */  <role8, t1, role21, t7, role19>
    /* 278 */  <role8, t2, role21, t4, role19>
    /* 279 */  <role8, t6, role21, t1, role19>
    /* 280 */  <role8, t4, role6, t8, role19>
    /* 281 */  <role8, t3, role6, t2, role19>
    /* 282 */  <TRUE, t7, role6, t3, role19>
    /* 283 */  <TRUE, t6, role6, t7, role19>
    /* 284 */  <role8, t10, role6, t4, role19>
    /* 285 */  <role8, t5, role6, t10, role19>
    /* 286 */  <TRUE, t8, role6, t5, role19>
    /* 287 */  <role8, t1, role6, t9, role19>
    /* 288 */  <role8, t1, role24, t2, role19>
    /* 289 */  <role8, t3, role23, t2, role19>
    /* 290 */  <role8, t10, role8, t8, role19>
    /* 291 */  <role8, t1, role8, t3, role19>
    /* 292 */  <role8, t9, role8, t10, role19>
    /* 293 */  <role8, t2, role8, t9, role19>
    /* 294 */  <TRUE, t3, role8, t7, role19>
    /* 295 */  <role8, t2, role21, t1, role10>
    /* 296 */  <role8, t10, role21, t4, role10>
    /* 297 */  <TRUE, t6, role21, t10, role10>
    /* 298 */  <role8, t8, role21, t3, role10>
    /* 299 */  <role8, t3, role24, t8, role10>
    /* 300 */  <role8, t1, role24, t9, role10>
    /* 301 */  <role8, t10, role24, t3, role10>
    /* 302 */  <role8, t9, role24, t1, role10>
    /* 303 */  <role8, t7, role24, t7, role10>
    /* 304 */  <role8, t8, role24, t6, role10>
    /* 305 */  <TRUE, t2, role24, t10, role10>
    /* 306 */  <role8, t4, role24, t2, role10>
    /* 307 */  <TRUE, t8, role23, t8, role10>
    /* 308 */  <role8, t9, role23, t5, role10>
    /* 309 */  <TRUE, t6, role23, t4, role10>
    /* 310 */  <role8, t7, role23, t3, role10>
    /* 311 */  <role8, t1, role23, t6, role10>
    /* 312 */  <role8, t2, role23, t7, role10>
    /* 313 */  <role8, t10, role23, t10, role10>
    /* 314 */  <role8, t3, role23, t1, role10>
    /* 315 */  <TRUE, t6, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t1, role24>
    /* 316 */  <role22, t7, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t2, role24>
    /* 317 */  <role22, t9, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t4, role24>
    /* 318 */  <role22, t2, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t5, role24>
    /* 319 */  <role24, t10, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t5, role6>
    /* 320 */  <role24, t6, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t10, role6>
    /* 321 */  <role24, t5, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t4, role6>
    /* 322 */  <TRUE, t1, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t6, role6>
    /* 323 */  <role24, t3, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t1, role6>
    /* 324 */  <role6, t9, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t6, role8>
    /* 325 */  <role6, t4, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t7, role8>
    /* 326 */  <TRUE, t5, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t9, role8>
    /* 327 */  <role6, t3, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t10, role8>
    /* 328 */  <role6, t7, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t2, role8>
    /* 329 */  <TRUE, t10, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t1, role8>
    /* 330 */  <TRUE, t8, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t3, role8>
    /* 331 */  <role6, t1, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t9, role26>
}

/* 
 * Number of Rules       : 139
 * Largest Precondition  : 0
 * Largest Role Schedule : 1
 * Startable Rules       : 139
 * Truly Startable Rules : 25
 */
CanRevoke {
    /*   1 */  <role6, t9, TRUE, t6, role32>
    /*   2 */  <role6, t6, TRUE, t4, role32>
    /*   3 */  <role6, t2, TRUE, t5, role32>
    /*   4 */  <role6, t4, TRUE, t1, role32>
    /*   5 */  <role6, t7, TRUE, t7, role32>
    /*   6 */  <role6, t8, TRUE, t3, role32>
    /*   7 */  <role6, t10, TRUE, t8, role32>
    /*   8 */  <role8, t9, TRUE, t7, role30>
    /*   9 */  <TRUE, t2, TRUE, t9, role30>
    /*  10 */  <role8, t5, TRUE, t3, role30>
    /*  11 */  <TRUE, t10, TRUE, t1, role30>
    /*  12 */  <role8, t3, TRUE, t6, role30>
    /*  13 */  <role8, t8, TRUE, t8, role30>
    /*  14 */  <role8, t4, TRUE, t2, role30>
    /*  15 */  <role8, t6, TRUE, t10, role30>
    /*  16 */  <TRUE, t8, TRUE, t3, role17>
    /*  17 */  <TRUE, t10, TRUE, t1, role17>
    /*  18 */  <role8, t1, TRUE, t2, role17>
    /*  19 */  <TRUE, t2, TRUE, t7, role17>
    /*  20 */  <role8, t3, TRUE, t4, role17>
    /*  21 */  <role8, t9, TRUE, t7, role16>
    /*  22 */  <role8, t5, TRUE, t9, role18>
    /*  23 */  <role8, t4, TRUE, t3, role18>
    /*  24 */  <role8, t9, TRUE, t4, role18>
    /*  25 */  <role6, t6, TRUE, t9, role14>
    /*  26 */  <role6, t3, TRUE, t4, role14>
    /*  27 */  <role6, t1, TRUE, t7, role14>
    /*  28 */  <role6, t9, TRUE, t1, role14>
    /*  29 */  <role6, t4, TRUE, t6, role14>
    /*  30 */  <role6, t7, TRUE, t5, role14>
    /*  31 */  <TRUE, t5, TRUE, t2, role14>
    /*  32 */  <role6, t8, TRUE, t8, role14>
    /*  33 */  <role19, t9, TRUE, t7, role20>
    /*  34 */  <role19, t2, TRUE, t3, role20>
    /*  35 */  <role19, t8, TRUE, t5, role20>
    /*  36 */  <role19, t7, TRUE, t6, role20>
    /*  37 */  <TRUE, t3, TRUE, t8, role20>
    /*  38 */  <TRUE, t10, TRUE, t2, role20>
    /*  39 */  <role19, t6, TRUE, t1, role20>
    /*  40 */  <role19, t1, TRUE, t4, role20>
    /*  41 */  <TRUE, t4, TRUE, t9, role20>
    /*  42 */  <role19, t5, TRUE, t10, role20>
    /*  43 */  <role12, t5, TRUE, t2, role33>
    /*  44 */  <TRUE, t8, TRUE, t5, role33>
    /*  45 */  <role12, t1, TRUE, t1, role33>
    /*  46 */  <role12, t2, TRUE, t6, role33>
    /*  47 */  <role12, t6, TRUE, t7, role33>
    /*  48 */  <role6, t1, TRUE, t4, role5>
    /*  49 */  <role6, t4, TRUE, t9, role5>
    /*  50 */  <role6, t2, TRUE, t3, role5>
    /*  51 */  <role6, t3, TRUE, t6, role5>
    /*  52 */  <role6, t7, TRUE, t5, role5>
    /*  53 */  <role6, t5, TRUE, t10, role5>
    /*  54 */  <role6, t6, TRUE, t8, role5>
    /*  55 */  <role6, t8, TRUE, t1, role5>
    /*  56 */  <role7, t10, TRUE, t8, role2>
    /*  57 */  <role7, t7, TRUE, t9, role2>
    /*  58 */  <role7, t4, TRUE, t10, role2>
    /*  59 */  <role7, t3, TRUE, t1, role2>
    /*  60 */  <role8, t10, TRUE, t9, role15>
    /*  61 */  <role8, t6, TRUE, t2, role15>
    /*  62 */  <TRUE, t1, TRUE, t7, role15>
    /*  63 */  <role8, t9, TRUE, t3, role15>
    /*  64 */  <role8, t4, TRUE, t8, role15>
    /*  65 */  <role8, t2, TRUE, t4, role15>
    /*  66 */  <role6, t7, TRUE, t9, role26>
    /*  67 */  <role8, t7, TRUE, t10, role15>
    /*  68 */  <role8, t3, TRUE, t1, role15>
    /*  69 */  <role8, t5, TRUE, t5, role15>
    /*  70 */  <role8, t8, TRUE, t6, role15>
    /*  71 */  <role8, t6, TRUE, t2, role16>
    /*  72 */  <TRUE, t5, TRUE, t3, role16>
    /*  73 */  <role8, t4, TRUE, t5, role16>
    /*  74 */  <role8, t3, TRUE, t6, role19>
    /*  75 */  <TRUE, t4, TRUE, t4, role19>
    /*  76 */  <role8, t7, TRUE, t3, role19>
    /*  77 */  <role8, t6, TRUE, t2, role19>
    /*  78 */  <role8, t5, TRUE, t7, role19>
    /*  79 */  <role8, t7, TRUE, t9, role26>
    /*  80 */  <role8, t9, TRUE, t10, role19>
    /*  81 */  <role8, t8, TRUE, t5, role19>
    /*  82 */  <role8, t1, TRUE, t9, role19>
    /*  83 */  <role8, t2, TRUE, t3, role10>
    /*  84 */  <role8, t3, TRUE, t2, role10>
    /*  85 */  <TRUE, t6, TRUE, t1, role10>
    /*  86 */  <role8, t5, TRUE, t5, role10>
    /*  87 */  <role8, t7, TRUE, t4, role10>
    /*  88 */  <role8, t9, TRUE, t9, role10>
    /*  89 */  <TRUE, t4, TRUE, t6, role10>
    /*  90 */  <role8, t10, TRUE, t7, role10>
    /*  91 */  <TRUE, t8, TRUE, t10, role10>
    /*  92 */  <role8, t5, TRUE, t1, role11>
    /*  93 */  <role8, t1, TRUE, t8, role25>
    /*  94 */  <role8, t7, TRUE, t9, role25>
    /*  95 */  <role8, t9, TRUE, t10, role25>
    /*  96 */  <TRUE, t3, TRUE, t1, role25>
    /*  97 */  <role8, t8, TRUE, t6, role25>
    /*  98 */  <role8, t10, TRUE, t2, role25>
    /*  99 */  <TRUE, t1, TRUE, t1, role26>
    /* 100 */  <role8, t9, TRUE, t2, role26>
    /* 101 */  <role8, t10, TRUE, t6, role26>
    /* 102 */  <role8, t6, TRUE, t3, role26>
    /* 103 */  <role8, t2, TRUE, t4, role26>
    /* 104 */  <TRUE, t7, TRUE, t9, role26>
    /* 105 */  <role8, t3, TRUE, t8, role26>
    /* 106 */  <role22, t4, TRUE, t1, role21>
    /* 107 */  <role22, t5, TRUE, t7, role21>
    /* 108 */  <role22, t9, TRUE, t2, role21>
    /* 109 */  <role22, t8, TRUE, t3, role21>
    /* 110 */  <role22, t3, TRUE, t9, role3>
    /* 111 */  <role22, t9, TRUE, t10, role3>
    /* 112 */  <TRUE, t8, TRUE, t7, role3>
    /* 113 */  <role22, t10, TRUE, t2, role3>
    /* 114 */  <role22, t6, TRUE, t8, role3>
    /* 115 */  <role22, t2, TRUE, t1, role3>
    /* 116 */  <role22, t1, TRUE, t3, role3>
    /* 117 */  <role22, t3, TRUE, t2, role4>
    /* 118 */  <role22, t5, TRUE, t3, role4>
    /* 119 */  <TRUE, t8, TRUE, t4, role4>
    /* 120 */  <role22, t4, TRUE, t1, role4>
    /* 121 */  <role22, t9, TRUE, t8, role4>
    /* 122 */  <role22, t2, TRUE, t5, role4>
    /* 123 */  <TRUE, t7, TRUE, t6, role4>
    /* 124 */  <role22, t10, TRUE, t7, role4>
    /* 125 */  <role22, t10, TRUE, t3, role24>
    /* 126 */  <TRUE, t2, TRUE, t4, role24>
    /* 127 */  <role22, t7, TRUE, t5, role24>
    /* 128 */  <role22, t9, TRUE, t7, role24>
    /* 129 */  <role22, t1, TRUE, t1, role24>
    /* 130 */  <TRUE, t4, TRUE, t8, role24>
    /* 131 */  <role22, t5, TRUE, t9, role24>
    /* 132 */  <TRUE, t4, TRUE, t3, role6>
    /* 133 */  <role6, t1, TRUE, t8, role8>
    /* 134 */  <role6, t5, TRUE, t3, role8>
    /* 135 */  <role6, t2, TRUE, t9, role8>
    /* 136 */  <role6, t3, TRUE, t4, role8>
    /* 137 */  <role6, t7, TRUE, t2, role8>
    /* 138 */  <role6, t6, TRUE, t10, role8>
    /* 139 */  <role6, t4, TRUE, t5, role8>
}

/* 
 * Number of Rules       : 8
 * Largest Precondition  : 4
 * Largest Role Schedule : 1
 * Startable Rules       : 2
 * Truly Startable Rules : 0
 */
CanEnable {
    /* 1 */  <role6, t8, TRUE, t3, role32>
    /* 2 */  <role8, t5, TRUE, t3, role30>
    /* 3 */  <role22, t8, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t6, role24>
    /* 4 */  <role24, t10, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t5, role6>
    /* 5 */  <role24, t5, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t4, role6>
    /* 6 */  <role6, t9, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t6, role8>
    /* 7 */  <role6, t6, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t8, role8>
    /* 8 */  <role6, t7, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t2, role8>
}

/* 
 * Number of Rules       : 5
 * Largest Precondition  : 4
 * Largest Role Schedule : 1
 * Startable Rules       : 4
 * Truly Startable Rules : 1
 */
CanDisable {
    /* 1 */  <role6, t2, TRUE, t5, role32>
    /* 2 */  <role6, t10, TRUE, t8, role32>
    /* 3 */  <role8, t9, TRUE, t7, role30>
    /* 4 */  <TRUE, t10, TRUE, t1, role30>
    /* 5 */  <TRUE, t5, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t9, role8>
}