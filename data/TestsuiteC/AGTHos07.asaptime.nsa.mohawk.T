/* Generated On        : 2015/02/07 20:24:43.514
 * Generated With      : Mohawk Reverse Converter: ASAPTime NSA
 * Number of Roles     : 15
 * Number of Timeslots : 30
 * Number of Rules     : 367
 * 
 * Roles     : |Size=15| [role1, role2, role3, role4, role5, role6, role7, role8, role9, role10, role11, role12, role13, role14, role15]
 * Timeslots : |Size=30| [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17, t18, t19, t20, t21, t22, t23, t24, t25, t26, t27, t28, t29, t30]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t14, [role9]

Expected: UNKNOWN

/* 
 * Number of Rules       : 181
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 135
 * Truly Startable Rules : 31
 */
CanAssign {
    /*   1 */  <role1, t4, role9 & role11, t30, role15>
    /*   2 */  <TRUE, t6, role9 & role11, t18, role15>
    /*   3 */  <role1, t24, role9 & role11, t29, role15>
    /*   4 */  <TRUE, t27, role9 & role11, t1, role15>
    /*   5 */  <role1, t28, role9 & role11, t20, role15>
    /*   6 */  <role1, t23, role9 & role11, t2, role15>
    /*   7 */  <role1, t19, role9 & role11, t11, role15>
    /*   8 */  <role1, t30, role9 & role11, t5, role15>
    /*   9 */  <role1, t8, role9 & role11, t19, role15>
    /*  10 */  <role1, t25, role9 & role11, t3, role15>
    /*  11 */  <role1, t7, role9 & role11, t4, role15>
    /*  12 */  <role1, t16, NOT ~ role11, t14, role9>
    /*  13 */  <TRUE, t9, role9 & role11, t9, role15>
    /*  14 */  <role3, t23, TRUE, t24, role14>
    /*  15 */  <role3, t26, TRUE, t11, role14>
    /*  16 */  <role3, t6, TRUE, t16, role14>
    /*  17 */  <role3, t24, TRUE, t12, role14>
    /*  18 */  <role3, t2, TRUE, t30, role14>
    /*  19 */  <TRUE, t10, TRUE, t13, role14>
    /*  20 */  <role3, t8, TRUE, t8, role14>
    /*  21 */  <role3, t1, TRUE, t20, role14>
    /*  22 */  <role3, t22, TRUE, t21, role14>
    /*  23 */  <role11, t16, NOT ~ role11, t14, role9>
    /*  24 */  <TRUE, t11, TRUE, t23, role4>
    /*  25 */  <role5, t20, TRUE, t25, role4>
    /*  26 */  <TRUE, t8, TRUE, t2, role4>
    /*  27 */  <TRUE, t7, TRUE, t27, role4>
    /*  28 */  <role5, t12, TRUE, t28, role4>
    /*  29 */  <role5, t22, TRUE, t22, role4>
    /*  30 */  <role5, t23, TRUE, t29, role4>
    /*  31 */  <role5, t26, TRUE, t3, role4>
    /*  32 */  <role5, t9, TRUE, t17, role4>
    /*  33 */  <role5, t2, TRUE, t30, role4>
    /*  34 */  <role6, t16, NOT ~ role11, t14, role9>
    /*  35 */  <TRUE, t17, TRUE, t12, role4>
    /*  36 */  <TRUE, t10, TRUE, t18, role4>
    /*  37 */  <TRUE, t28, TRUE, t1, role4>
    /*  38 */  <role5, t11, TRUE, t1, role6>
    /*  39 */  <role5, t17, TRUE, t6, role6>
    /*  40 */  <role5, t18, TRUE, t13, role6>
    /*  41 */  <role5, t26, TRUE, t28, role6>
    /*  42 */  <role5, t28, TRUE, t11, role6>
    /*  43 */  <role12, t17, NOT ~ role11, t14, role9>
    /*  44 */  <TRUE, t25, TRUE, t9, role6>
    /*  45 */  <TRUE, t19, TRUE, t8, role6>
    /*  46 */  <role5, t20, TRUE, t16, role6>
    /*  47 */  <role5, t27, TRUE, t27, role6>
    /*  48 */  <role5, t17, TRUE, t15, role8>
    /*  49 */  <TRUE, t7, TRUE, t9, role8>
    /*  50 */  <role5, t8, TRUE, t1, role8>
    /*  51 */  <role5, t22, TRUE, t4, role8>
    /*  52 */  <TRUE, t21, TRUE, t8, role8>
    /*  53 */  <role5, t14, TRUE, t23, role8>
    /*  54 */  <role5, t9, TRUE, t13, role8>
    /*  55 */  <role5, t10, TRUE, t11, role8>
    /*  56 */  <TRUE, t23, TRUE, t10, role8>
    /*  57 */  <role5, t26, TRUE, t27, role8>
    /*  58 */  <TRUE, t6, TRUE, t17, role8>
    /*  59 */  <role5, t28, TRUE, t12, role8>
    /*  60 */  <role5, t11, TRUE, t6, role8>
    /*  61 */  <role5, t24, TRUE, t28, role8>
    /*  62 */  <role5, t12, TRUE, t21, role8>
    /*  63 */  <role5, t29, TRUE, t16, role8>
    /*  64 */  <role5, t25, TRUE, t22, role8>
    /*  65 */  <role5, t13, TRUE, t14, role8>
    /*  66 */  <role5, t16, NOT ~ role11, t14, role9>
    /*  67 */  <role9, t18, TRUE, t5, role2>
    /*  68 */  <role9, t6, TRUE, t8, role2>
    /*  69 */  <role9, t12, TRUE, t4, role2>
    /*  70 */  <role9, t15, TRUE, t23, role2>
    /*  71 */  <role9, t2, TRUE, t16, role2>
    /*  72 */  <role9, t13, TRUE, t24, role2>
    /*  73 */  <role12, t12, NOT ~ role11, t14, role9>
    /*  74 */  <TRUE, t11, TRUE, t22, role2>
    /*  75 */  <TRUE, t19, TRUE, t1, role2>
    /*  76 */  <role9, t29, TRUE, t3, role2>
    /*  77 */  <TRUE, t14, TRUE, t7, role2>
    /*  78 */  <TRUE, t28, TRUE, t21, role2>
    /*  79 */  <role9, t23, TRUE, t9, role2>
    /*  80 */  <role9, t10, TRUE, t17, role2>
    /*  81 */  <role9, t30, TRUE, t13, role2>
    /*  82 */  <role9, t1, TRUE, t10, role2>
    /*  83 */  <role9, t20, TRUE, t6, role2>
    /*  84 */  <TRUE, t3, TRUE, t19, role2>
    /*  85 */  <TRUE, t21, TRUE, t11, role2>
    /*  86 */  <role9, t22, TRUE, t25, role2>
    /*  87 */  <role9, t24, TRUE, t12, role2>
    /*  88 */  <role10, t16, NOT ~ role11, t14, role9>
    /*  89 */  <role3, t18, TRUE, t24, role14>
    /*  90 */  <role3, t1, TRUE, t29, role14>
    /*  91 */  <TRUE, t30, TRUE, t10, role14>
    /*  92 */  <role3, t22, TRUE, t3, role14>
    /*  93 */  <role3, t24, TRUE, t11, role14>
    /*  94 */  <role5, t7, TRUE, t25, role4>
    /*  95 */  <role5, t30, TRUE, t1, role4>
    /*  96 */  <role5, t17, TRUE, t11, role4>
    /*  97 */  <role5, t26, TRUE, t10, role4>
    /*  98 */  <role5, t15, TRUE, t12, role4>
    /*  99 */  <role5, t20, TRUE, t3, role4>
    /* 100 */  <TRUE, t23, TRUE, t28, role4>
    /* 101 */  <role5, t21, TRUE, t4, role4>
    /* 102 */  <TRUE, t10, TRUE, t15, role4>
    /* 103 */  <TRUE, t29, TRUE, t9, role4>
    /* 104 */  <role5, t12, TRUE, t23, role4>
    /* 105 */  <role5, t9, TRUE, t18, role4>
    /* 106 */  <role5, t28, TRUE, t6, role4>
    /* 107 */  <role5, t19, TRUE, t5, role4>
    /* 108 */  <role5, t1, TRUE, t13, role4>
    /* 109 */  <role5, t13, TRUE, t7, role4>
    /* 110 */  <role5, t11, TRUE, t14, role4>
    /* 111 */  <role5, t24, TRUE, t21, role4>
    /* 112 */  <role5, t27, TRUE, t8, role4>
    /* 113 */  <role5, t11, TRUE, t17, role6>
    /* 114 */  <role5, t5, TRUE, t19, role6>
    /* 115 */  <role5, t6, TRUE, t24, role6>
    /* 116 */  <role5, t13, TRUE, t27, role6>
    /* 117 */  <TRUE, t17, TRUE, t12, role6>
    /* 118 */  <role5, t20, TRUE, t16, role8>
    /* 119 */  <role9, t26, TRUE, t12, role2>
    /* 120 */  <role9, t2, TRUE, t10, role2>
    /* 121 */  <TRUE, t7, TRUE, t17, role2>
    /* 122 */  <TRUE, t13, TRUE, t1, role2>
    /* 123 */  <role9, t25, TRUE, t28, role2>
    /* 124 */  <TRUE, t24, TRUE, t8, role2>
    /* 125 */  <role3, t8, role3, t15, role13>
    /* 126 */  <role3, t21, role3, t17, role13>
    /* 127 */  <TRUE, t23, role3, t24, role13>
    /* 128 */  <role3, t16, role3, t2, role13>
    /* 129 */  <role3, t7, role3, t14, role13>
    /* 130 */  <TRUE, t22, role3, t18, role13>
    /* 131 */  <role3, t17, role3, t5, role13>
    /* 132 */  <TRUE, t23, role3, t24, role7>
    /* 133 */  <TRUE, t3, role3, t2, role7>
    /* 134 */  <role6, t25, role3, t15, role7>
    /* 135 */  <TRUE, t24, role3, t20, role7>
    /* 136 */  <role6, t26, role3, t17, role7>
    /* 137 */  <role6, t30, role3, t12, role7>
    /* 138 */  <TRUE, t27, role3, t4, role7>
    /* 139 */  <role6, t17, role3, t13, role7>
    /* 140 */  <role6, t13, role3, t5, role7>
    /* 141 */  <role6, t18, role8, t20, role7>
    /* 142 */  <role6, t19, role8, t9, role7>
    /* 143 */  <role6, t14, role8, t29, role7>
    /* 144 */  <TRUE, t1, role8, t16, role7>
    /* 145 */  <role6, t13, role8, t12, role7>
    /* 146 */  <role6, t17, role8, t14, role7>
    /* 147 */  <role6, t8, role8, t26, role7>
    /* 148 */  <role6, t20, role8, t15, role7>
    /* 149 */  <role5, t14, NOT ~ role3, t23, role12>
    /* 150 */  <role5, t22, NOT ~ role3, t5, role12>
    /* 151 */  <role5, t19, NOT ~ role3, t19, role12>
    /* 152 */  <role5, t20, NOT ~ role3, t13, role12>
    /* 153 */  <TRUE, t6, NOT ~ role12, t20, role3>
    /* 154 */  <role5, t24, NOT ~ role12, t5, role3>
    /* 155 */  <role5, t17, NOT ~ role12, t24, role3>
    /* 156 */  <TRUE, t9, role3 & NOT ~ role9, t22, role11>
    /* 157 */  <TRUE, t20, role3 & NOT ~ role9, t18, role11>
    /* 158 */  <role9, t16, role3 & NOT ~ role9, t20, role11>
    /* 159 */  <role9, t23, role3 & NOT ~ role9, t30, role11>
    /* 160 */  <role9, t11, role3 & NOT ~ role9, t23, role11>
    /* 161 */  <role9, t14, role3 & NOT ~ role9, t21, role11>
    /* 162 */  <role9, t4, role3 & NOT ~ role9, t2, role11>
    /* 163 */  <role9, t24, role3 & NOT ~ role9, t24, role11>
    /* 164 */  <role9, t16, NOT ~ role11, t14, role9>
    /* 165 */  <TRUE, t11, NOT ~ role11, t18, role9>
    /* 166 */  <role12, t15, NOT ~ role11, t12, role9>
    /* 167 */  <role12, t9, NOT ~ role11, t29, role9>
    /* 168 */  <role12, t28, NOT ~ role11, t19, role9>
    /* 169 */  <role12, t7, NOT ~ role11, t20, role9>
    /* 170 */  <TRUE, t6, NOT ~ role11, t1, role9>
    /* 171 */  <role12, t5, NOT ~ role11, t6, role9>
    /* 172 */  <role12, t18, NOT ~ role11, t8, role9>
    /* 173 */  <role12, t17, NOT ~ role11, t21, role9>
    /* 174 */  <TRUE, t24, NOT ~ role11, t22, role9>
    /* 175 */  <role12, t25, NOT ~ role11, t28, role9>
    /* 176 */  <role12, t21, NOT ~ role11, t9, role9>
    /* 177 */  <role12, t30, NOT ~ role11, t13, role9>
    /* 178 */  <role12, t16, NOT ~ role11, t14, role9>
    /* 179 */  <role12, t26, NOT ~ role11, t25, role9>
    /* 180 */  <role14, t2, role9, t14, role10>
    /* 181 */  <role14, t26, role9, t28, role10>
}

/* 
 * Number of Rules       : 111
 * Largest Precondition  : 0
 * Largest Role Schedule : 1
 * Startable Rules       : 111
 * Truly Startable Rules : 17
 */
CanRevoke {
    /*   1 */  <role3, t8, TRUE, t29, role14>
    /*   2 */  <role3, t11, TRUE, t12, role14>
    /*   3 */  <role3, t29, TRUE, t3, role13>
    /*   4 */  <role3, t14, TRUE, t29, role13>
    /*   5 */  <TRUE, t7, TRUE, t21, role13>
    /*   6 */  <role3, t28, TRUE, t11, role13>
    /*   7 */  <role6, t10, TRUE, t17, role7>
    /*   8 */  <role6, t9, TRUE, t18, role7>
    /*   9 */  <role6, t13, TRUE, t6, role7>
    /*  10 */  <role6, t14, TRUE, t20, role7>
    /*  11 */  <role6, t8, TRUE, t23, role7>
    /*  12 */  <TRUE, t2, TRUE, t19, role7>
    /*  13 */  <role6, t11, TRUE, t28, role7>
    /*  14 */  <role6, t15, TRUE, t25, role7>
    /*  15 */  <role6, t21, TRUE, t26, role7>
    /*  16 */  <role6, t3, TRUE, t9, role7>
    /*  17 */  <role6, t22, TRUE, t7, role7>
    /*  18 */  <role6, t23, TRUE, t8, role7>
    /*  19 */  <TRUE, t12, TRUE, t16, role7>
    /*  20 */  <role6, t24, TRUE, t27, role7>
    /*  21 */  <role6, t2, TRUE, t4, role7>
    /*  22 */  <TRUE, t22, TRUE, t8, role7>
    /*  23 */  <role6, t3, TRUE, t7, role7>
    /*  24 */  <role6, t30, TRUE, t24, role7>
    /*  25 */  <role6, t17, TRUE, t9, role7>
    /*  26 */  <role6, t6, TRUE, t10, role7>
    /*  27 */  <TRUE, t19, TRUE, t14, role7>
    /*  28 */  <role6, t1, TRUE, t13, role7>
    /*  29 */  <role6, t15, TRUE, t5, role7>
    /*  30 */  <role6, t4, TRUE, t11, role7>
    /*  31 */  <role6, t11, TRUE, t25, role7>
    /*  32 */  <role5, t12, TRUE, t16, role4>
    /*  33 */  <role5, t6, TRUE, t14, role4>
    /*  34 */  <role5, t7, TRUE, t6, role4>
    /*  35 */  <role5, t23, TRUE, t13, role4>
    /*  36 */  <role5, t14, TRUE, t10, role4>
    /*  37 */  <role5, t1, TRUE, t26, role4>
    /*  38 */  <role5, t13, TRUE, t20, role4>
    /*  39 */  <TRUE, t15, TRUE, t22, role4>
    /*  40 */  <role5, t19, TRUE, t27, role4>
    /*  41 */  <role5, t8, TRUE, t23, role4>
    /*  42 */  <role5, t29, TRUE, t11, role4>
    /*  43 */  <role6, t10, TRUE, t14, role9>
    /*  44 */  <role5, t19, TRUE, t7, role6>
    /*  45 */  <role5, t13, TRUE, t10, role6>
    /*  46 */  <role5, t23, TRUE, t22, role6>
    /*  47 */  <role5, t3, TRUE, t9, role12>
    /*  48 */  <role5, t1, TRUE, t27, role12>
    /*  49 */  <role5, t4, TRUE, t18, role12>
    /*  50 */  <TRUE, t23, TRUE, t28, role12>
    /*  51 */  <role5, t20, TRUE, t30, role12>
    /*  52 */  <role5, t14, TRUE, t5, role12>
    /*  53 */  <TRUE, t12, TRUE, t4, role12>
    /*  54 */  <TRUE, t18, TRUE, t6, role12>
    /*  55 */  <role5, t19, TRUE, t8, role12>
    /*  56 */  <role5, t13, TRUE, t24, role12>
    /*  57 */  <TRUE, t21, TRUE, t26, role12>
    /*  58 */  <role5, t6, TRUE, t7, role12>
    /*  59 */  <role5, t16, TRUE, t10, role12>
    /*  60 */  <TRUE, t24, TRUE, t11, role12>
    /*  61 */  <role5, t28, TRUE, t12, role12>
    /*  62 */  <TRUE, t22, TRUE, t3, role8>
    /*  63 */  <role5, t12, TRUE, t18, role8>
    /*  64 */  <role5, t23, TRUE, t23, role8>
    /*  65 */  <role5, t24, TRUE, t7, role3>
    /*  66 */  <role5, t10, TRUE, t14, role9>
    /*  67 */  <role5, t25, TRUE, t11, role3>
    /*  68 */  <TRUE, t3, TRUE, t23, role3>
    /*  69 */  <role9, t27, TRUE, t22, role2>
    /*  70 */  <role9, t17, TRUE, t29, role2>
    /*  71 */  <role9, t24, TRUE, t4, role2>
    /*  72 */  <role9, t13, TRUE, t23, role2>
    /*  73 */  <role9, t7, TRUE, t20, role2>
    /*  74 */  <role9, t6, TRUE, t24, role2>
    /*  75 */  <role9, t3, TRUE, t30, role2>
    /*  76 */  <role9, t28, TRUE, t12, role2>
    /*  77 */  <role9, t19, TRUE, t6, role2>
    /*  78 */  <role9, t18, TRUE, t1, role2>
    /*  79 */  <role9, t10, TRUE, t2, role2>
    /*  80 */  <role9, t25, TRUE, t28, role2>
    /*  81 */  <role9, t10, TRUE, t14, role9>
    /*  82 */  <role9, t22, TRUE, t4, role11>
    /*  83 */  <role9, t4, TRUE, t20, role11>
    /*  84 */  <role9, t29, TRUE, t13, role11>
    /*  85 */  <role3, t22, TRUE, t7, role9>
    /*  86 */  <role3, t11, TRUE, t10, role9>
    /*  87 */  <role3, t1, TRUE, t17, role9>
    /*  88 */  <role3, t24, TRUE, t15, role9>
    /*  89 */  <role3, t15, TRUE, t5, role9>
    /*  90 */  <TRUE, t8, TRUE, t23, role9>
    /*  91 */  <role3, t27, TRUE, t8, role9>
    /*  92 */  <TRUE, t7, TRUE, t2, role9>
    /*  93 */  <TRUE, t16, TRUE, t26, role9>
    /*  94 */  <role3, t9, TRUE, t6, role9>
    /*  95 */  <role3, t19, TRUE, t9, role9>
    /*  96 */  <role3, t25, TRUE, t11, role9>
    /*  97 */  <role3, t10, TRUE, t14, role9>
    /*  98 */  <role3, t17, TRUE, t3, role9>
    /*  99 */  <role3, t2, TRUE, t4, role9>
    /* 100 */  <TRUE, t12, TRUE, t24, role9>
    /* 101 */  <role3, t13, TRUE, t18, role9>
    /* 102 */  <role3, t18, TRUE, t12, role9>
    /* 103 */  <role3, t14, TRUE, t13, role9>
    /* 104 */  <role3, t4, TRUE, t16, role9>
    /* 105 */  <role14, t21, TRUE, t14, role10>
    /* 106 */  <role14, t17, TRUE, t24, role10>
    /* 107 */  <role14, t22, TRUE, t27, role10>
    /* 108 */  <role14, t18, TRUE, t13, role10>
    /* 109 */  <role14, t3, TRUE, t15, role10>
    /* 110 */  <role14, t16, TRUE, t7, role10>
    /* 111 */  <role14, t1, TRUE, t20, role10>
}

/* 
 * Number of Rules       : 52
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 42
 * Truly Startable Rules : 7
 */
CanEnable {
    /*  1 */  <role3, t11, TRUE, t12, role14>
    /*  2 */  <role6, t10, TRUE, t17, role7>
    /*  3 */  <role6, t14, TRUE, t20, role7>
    /*  4 */  <role6, t15, TRUE, t25, role7>
    /*  5 */  <TRUE, t12, TRUE, t16, role7>
    /*  6 */  <role6, t24, TRUE, t27, role7>
    /*  7 */  <role6, t2, TRUE, t4, role7>
    /*  8 */  <role6, t3, TRUE, t7, role7>
    /*  9 */  <role6, t30, TRUE, t24, role7>
    /* 10 */  <TRUE, t19, TRUE, t14, role7>
    /* 11 */  <role6, t1, TRUE, t13, role7>
    /* 12 */  <role6, t11, TRUE, t25, role7>
    /* 13 */  <role5, t19, TRUE, t7, role6>
    /* 14 */  <role5, t23, TRUE, t22, role6>
    /* 15 */  <role5, t4, TRUE, t18, role12>
    /* 16 */  <role5, t16, TRUE, t10, role12>
    /* 17 */  <role5, t12, TRUE, t18, role8>
    /* 18 */  <role9, t6, TRUE, t24, role2>
    /* 19 */  <role9, t3, TRUE, t30, role2>
    /* 20 */  <role9, t22, TRUE, t4, role11>
    /* 21 */  <role3, t1, TRUE, t17, role9>
    /* 22 */  <TRUE, t7, TRUE, t2, role9>
    /* 23 */  <TRUE, t16, TRUE, t26, role9>
    /* 24 */  <role3, t2, TRUE, t4, role9>
    /* 25 */  <role3, t4, TRUE, t16, role9>
    /* 26 */  <role14, t17, TRUE, t24, role10>
    /* 27 */  <role14, t22, TRUE, t27, role10>
    /* 28 */  <role14, t18, TRUE, t13, role10>
    /* 29 */  <role14, t16, TRUE, t7, role10>
    /* 30 */  <TRUE, t27, role9 & role11, t1, role15>
    /* 31 */  <role1, t23, role9 & role11, t2, role15>
    /* 32 */  <role1, t19, role9 & role11, t11, role15>
    /* 33 */  <role1, t30, role9 & role11, t5, role15>
    /* 34 */  <role9, t1, TRUE, t10, role2>
    /* 35 */  <role3, t1, TRUE, t29, role14>
    /* 36 */  <role5, t7, TRUE, t25, role4>
    /* 37 */  <TRUE, t29, TRUE, t9, role4>
    /* 38 */  <role5, t19, TRUE, t5, role4>
    /* 39 */  <role5, t1, TRUE, t13, role4>
    /* 40 */  <role5, t11, TRUE, t14, role4>
    /* 41 */  <role5, t11, TRUE, t17, role6>
    /* 42 */  <role5, t6, TRUE, t24, role6>
    /* 43 */  <TRUE, t7, TRUE, t17, role2>
    /* 44 */  <TRUE, t13, TRUE, t1, role2>
    /* 45 */  <role3, t9, role3, t20, role13>
    /* 46 */  <TRUE, t23, role3, t24, role7>
    /* 47 */  <role6, t25, role3, t15, role7>
    /* 48 */  <role5, t24, NOT ~ role12, t5, role3>
    /* 49 */  <TRUE, t9, role3 & NOT ~ role9, t22, role11>
    /* 50 */  <role9, t24, role3 & NOT ~ role9, t24, role11>
    /* 51 */  <role12, t25, NOT ~ role11, t28, role9>
    /* 52 */  <role14, t2, role9, t14, role10>
}

/* 
 * Number of Rules       : 23
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 19
 * Truly Startable Rules : 4
 */
CanDisable {
    /*  1 */  <role3, t8, TRUE, t29, role14>
    /*  2 */  <role6, t11, TRUE, t28, role7>
    /*  3 */  <role6, t23, TRUE, t8, role7>
    /*  4 */  <role5, t23, TRUE, t13, role4>
    /*  5 */  <role5, t1, TRUE, t26, role4>
    /*  6 */  <role5, t19, TRUE, t27, role4>
    /*  7 */  <role5, t29, TRUE, t11, role4>
    /*  8 */  <role5, t13, TRUE, t10, role6>
    /*  9 */  <role5, t19, TRUE, t8, role12>
    /* 10 */  <TRUE, t24, TRUE, t11, role12>
    /* 11 */  <role5, t23, TRUE, t23, role8>
    /* 12 */  <role9, t17, TRUE, t29, role2>
    /* 13 */  <role1, t4, role9 & role11, t30, role15>
    /* 14 */  <TRUE, t21, TRUE, t11, role2>
    /* 15 */  <role5, t30, TRUE, t1, role4>
    /* 16 */  <role5, t15, TRUE, t12, role4>
    /* 17 */  <role5, t12, TRUE, t23, role4>
    /* 18 */  <role5, t5, TRUE, t19, role6>
    /* 19 */  <role3, t17, role3, t5, role13>
    /* 20 */  <role6, t26, role3, t17, role7>
    /* 21 */  <TRUE, t27, role3, t4, role7>
    /* 22 */  <TRUE, t6, NOT ~ role12, t20, role3>
    /* 23 */  <TRUE, t24, NOT ~ role11, t22, role9>
}