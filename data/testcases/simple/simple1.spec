/* Taken from sharelatex's "Working Files/Mohawk Examples/old-simple-tests.txt"
 * 
 * Converted simple_test1.txt into the new format
 */
CanAssign {
    /* <Admin role, 
     * starting timeslot - end timeslot for admin role, 
     * role preconditions, 
     * [time slot rule gives role to user for, another time slot (optional)...],
     * role to give to the user>
     */
    <role0,t0,TRUE,[t0],role1>		//(1)
    <role0,t0,role1,[t0],role2>		//(2)
}

CanRevoke {}

CanEnable {}

CanDisable {}

Query: t0,[role2]

Expected: TRUE