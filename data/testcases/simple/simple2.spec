/* Taken from sharelatex's "Working Files/Mohawk Examples/old-simple-tests.txt"
 * 
 * Converted simple_test2.txt into the new format
 */
Query: t1,[role2]
Expected: TRUE

CanAssign {
    /* <Admin role, 
     * starting timeslot - end timeslot for admin role, 
     * role preconditions, 
     * [time slot rule gives role to user for, another time slot (optional)...],
     * role to give to the user>
     */
    <role0,t0,TRUE,[t1],role0>		//(1)
    <role0,t1,TRUE,[t1],role1>		//(2)
    <role0,t0,role1,[t1],role2>		//(3)
}

CanRevoke {}

CanEnable {}

CanDisable {}
