package mohawk.converter.to;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import mohawk.global.FileExtensions;
import mohawk.global.pieces.MohawkT;
import mohawk.global.timing.MohawkTiming;

public class ConvertTo {
    public FileExtensions fileExt = new FileExtensions(); // allows for custom file extensions
    public String convertedStr = null;
    public String lastError = null;
    public Integer numberOfRules = null;
    public MohawkTiming timing = null;

    protected String tPrefix = "ConvertTo";

    public ConvertTo(MohawkTiming timing) {
        this.timing = timing;
    }

    /** Takes in a Mohawk+T Policy and converts it to an equivalent problem instance. Has the ability to then write that
     * instance out to a file, when {@code writeToFile} is TRUE.
     * 
     * @param m Input Mohawk+T policy to convert
     * @param f can be <b>NULL</b> if {@code writeToFile} is FALSE
     * @param writeToFile TRUE to write the instance out to the file {@code f}
     * @return the equivalent instance string */
    public String convert(MohawkT m, File f, Boolean writeToFile) {
        return null;
    }

    /** Returns the file extension that {@link ConvertTo#convert(MohawkT, File, Boolean)} will append to the file when
     * writing out
     * 
     * @return */
    public String getFileExtenstion() {
        return null;
    }

    // public File getTemplateFile() throws IOException;

    public static File getTemplateFile(String filename) throws IOException {
        File templateFile = new File(filename);

        if (!templateFile.exists()) { throw new IOException("Unable to find template file: " + filename); }
        return templateFile;
    }

    public static String readFile(URL url, Charset encoding) throws IOException, URISyntaxException {
        byte[] encoded = Files.readAllBytes(Paths.get(url.toURI()));
        return new String(encoded, encoding);
    }

    public static String readFile(URL url) throws IOException, URISyntaxException {
        return readFile(url, Charset.defaultCharset());
    }

}
