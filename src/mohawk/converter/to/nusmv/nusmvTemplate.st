MODULE main

-- Number of CA Rules: <numCARules>
-- Number of CR Rules: <numCRRules>
-- Number of CE Rules: <numCERules>
-- Number of CD Rules: <numCDRules>

DEFINE
numroles := <numRoles>;
numadminroles := <numAdminRoles>;  -- Might be used later as a optimization
numusers := <numUsers>; -- should be = numroles + 1
numtimeslots := <numTimeSlots>;

-- LTLSPEC: "G p" means that a certain condition p holds in all future time instants
-- <queryStr>
LTLSPEC G !(<goalRoles:{r|u[1]<r.string> = TRUE}; separator=" & ">)

VAR
user : 1 .. numusers;
admin: 1 .. numusers;

u : array 1..numusers of 
    array 1..numroles of 
    array 1..numtimeslots of boolean;
e : array 1..numroles of 
     array 1..numtimeslots of boolean;

rule : {
    -- CanAssign
	<caruleslist>
	-- CanRevoke
	<crruleslist>
	-- CanEnable
	<ceruleslist>
	-- CanDisable
	<cdruleslist>
};

ASSIGN

<users:{u|<roleTimeslotRules:{r|
<if(r.timeslots)>
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- User <u>, Role <r.role> Assign/Revoke 
<r.timeslots:{ts|
<if(ts.cases)>
next(u[<u>][<r.role>][<ts.timeslot>]) := case
<ts.cases:{c|<\t>user=<u> & <c.condition> : <if(c.trueResult)>TRUE<else>FALSE<endif>;}; separator="\n">
<\t>TRUE : u[<u>][<r.role>][<ts.timeslot>];
esac;
<else>next(u[<u>][<r.role>][<ts.timeslot>]) := FALSE; --u[<u>][<r.role>][<ts.timeslot>];
<endif>
}; separator="\n">
<else>-- Skipping <r.role>
<endif>
}; separator="\n">}; separator="\n">

<enabledRoleRules:{r|
<if(r.timeslots)>
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Role <r.role> Enable/Disable
<r.timeslots:{ts|
<if(ts.cases)>
next(e[<r.role>][<ts.timeslot>]) := case
<ts.cases:{c|<\t><c.condition> : <if(c.trueResult)>TRUE<else>FALSE<endif>;}; separator="\n">
<\t>TRUE : e[<r.role>][<ts.timeslot>];
esac;
<else>next(e[<r.role>][<ts.timeslot>]) := FALSE; --e[<r.role>][<ts.timeslot>];
<endif>
}; separator="\n">
<else>-- Skipping <r.role>
<endif>
}; separator="\n">


<!
-------------------------------------------------------- 
-- TODO: Figure out admin conditions
VAR
-- <numUsers> Users Variable
<users:{u|<roleTimeslots:{rt|U<u><rt.string> : boolean;}; separator="  ">}; separator="\n">

-- <numRolesEnabled> RoleEnabled
<enabledRoles:{rt|e<rt.string> : boolean;}; separator="  ">
------------------------------------------------------------
!>

ASSIGN
-- Disabling User Role Assignments
<users:{u|<roleTimeslots:{rt|init(u[<u>]<rt.string>) := FALSE;}; separator="  ">}; separator="\n">

-- Disabling Roles
<enabledRoles:{rt|init(e<rt.string>) := FALSE;}; separator="  ">