<! Roles role0 role1 role2 role3; !>
Roles <roles:{r|<r.Name>}; separator=" "> <adminrole>;

<! Users user0 user1 user2 user3 user4; !>
Users <users; separator=" ">;

<! UA <user4,Admin>; !>
UA \<<adminuser>,<adminrole>>;

<! CR <Admin,role0> <Admin,role2>; !>
CR <canrevoke:{cr|\<<adminrole>,<cr.role>>}; separator="\n\t">;

<! CA <role3,role2,role0> <role3,role2&role1,role0> <role3,role2&role0,role1> <role3,TRUE,role1> <role3,role0,role1>; !>
CA <canassign:{ca|\<<adminrole>,<if(ca.precondition)><ca.precondition; separator="&"><else>TRUE<endif>,<ca.role>>}; separator="\n\t">;

<! ADMIN user4; !>
ADMIN <adminuser>;

<! SPEC user0 role1; !>
SPEC <specuser> <specrole>;
