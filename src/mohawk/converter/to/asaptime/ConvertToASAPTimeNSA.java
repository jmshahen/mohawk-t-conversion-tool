package mohawk.converter.to.asaptime;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.logging.Logger;

import org.stringtemplate.v4.ST;

import mohawk.converter.to.ConvertTo;
import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;
import mohawk.global.pieces.reduced.query.ASAPTimeNSA_Query;
import mohawk.global.pieces.reduced.rules.ASAPTimeNSA_Rule;
import mohawk.global.timing.MohawkTiming;

public class ConvertToASAPTimeNSA extends ConvertTo {
    public static final Logger logger = Logger.getLogger("mohawk");

    public ConvertToASAPTimeNSA(MohawkTiming timing) {
        super(timing);
        tPrefix = "ConvertToASAPTimeNSA";
    }

    @Override
    public String convert(MohawkT m, File file, Boolean writeToFile) {
        try {
            ArrayList<ASAPTimeNSA_Rule> rules = new ArrayList<ASAPTimeNSA_Rule>();

            // Deep Copy Helpers
            /* Timing */timing.startTimer(tPrefix + "_" + "deepcopy");
            RoleHelper roleHelper = new RoleHelper(m.roleHelper);
            TimeIntervalHelper timeIntervalHelper = new TimeIntervalHelper(m.timeIntervalHelper);
            Query workableQuery = new Query(m.query);
            /* Timing */timing.stopTimer(tPrefix + "_" + "deepcopy");

            /* Timing */timing.startTimer(tPrefix + "_" + "sortRolesReduceTimeslots");
            // Reduce Roles to Integers
            roleHelper.allowZeroRole = false;// roles start from 1 NOT 0!
            roleHelper.setupSortedRoles();
            // Reduce TimeIntervals to Timeslots
            timeIntervalHelper.allowZeroTimeslot = false;// time-slots start from t1 NOT t0!
            // Reduction (2)
            timeIntervalHelper.reduceToTimeslots();
            /* Timing */timing.stopTimer(tPrefix + "_" + "sortRolesReduceTimeslots");

            // Convert Query to Reduced ASAPTime NSA Query and add any extra rules as needed
            // Reduction (1)
            ASAPTimeNSA_Query query = toASAPTimeNSA_Query(workableQuery, rules, roleHelper, timeIntervalHelper);

            // Convert Rules into Reduced ASAPTime NSA Rules
            /* Timing */timing.startTimer(tPrefix + "_" + "toASAPTimeNSA_Rules");
            for (Rule r : m.getAllRules()) {
                rules.addAll(toASAPTimeNSA_Rules(r, roleHelper, timeIntervalHelper));
            }
            /* Timing */timing.stopTimer(tPrefix + "_" + "toASAPTimeNSA_Rules");

            // Stats and logging
            numberOfRules = rules.size();
            logger.fine("Rules: " + m.getAllRules());
            logger.fine("Reduced Rules: " + rules);

            // Generate the Converted String
            /* Timing */timing.startTimer(tPrefix + "_" + "template");
            String template = ConvertTo.readFile(this.getClass().getResource("ASAPTimeNSATemplate.st"));
            ST st = new ST(template);
            st.add("numRoles", roleHelper.numberOfRoles());
            st.add("numTimeslots", timeIntervalHelper.sizeReduced());
            st.add("goalRole", query.goalRole);
            st.add("goalTimeslot", query.goalTimeslot);
            st.add("rules_nsa", rules);
            convertedStr = st.render();
            /* Timing */timing.stopTimer(tPrefix + "_" + "template");

            // Write the converted string out to "file + getFileExtenstion()"
            if (writeToFile) {
                /* Timing */timing.startTimer(tPrefix + "_" + "Files.write");
                File convertedFile = new File(file.getAbsolutePath() + getFileExtenstion());

                if (!convertedFile.exists()) {
                    convertedFile.createNewFile();
                }
                Files.write(convertedFile.toPath(), convertedStr.getBytes());
                /* Timing */timing.stopTimer(tPrefix + "_" + "Files.write");
            }
            lastError = null;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.fine(errors.toString());

            logger.warning("[ERROR] Unable to convert to Ranise: " + e.getMessage());
            lastError = "Error ConvertToRanise.convert.Exception #1";
        }

        return convertedStr;
    }

    /** Directly converts the Query's timeslot to the reduced timeslot and then it copies the Goal Role is there is only
     * 1. If there is more than 1 Goal Role in the Query then it will create a new Goal Role that is unique and create a
     * new Rule such that only someone who has reached the Goal Roles in the original Query can assign this new Goal
     * Role and thus make the system REACHABLE.
     * 
     * From the paper, this is the reduction (1) Query, Type 1
     * 
     * WARNING: if this function has to create a new role then it will call roleHelper.setupSortedRoles();
     * 
     * @param query
     * @param rules
     * @param roleHelper
     * @param timeIntervalHelper
     * @return */
    public ASAPTimeNSA_Query toASAPTimeNSA_Query(Query query, ArrayList<ASAPTimeNSA_Rule> rules, RoleHelper roleHelper,
            TimeIntervalHelper timeIntervalHelper) {
        ASAPTimeNSA_Query q = new ASAPTimeNSA_Query();

        // Set the Goal Timeslot
        SortedSet<Integer> goalTimeslots = timeIntervalHelper.indexOfReduced(query._timeslot);
        if (goalTimeslots.size() != 1) {
            logger.severe("goalTimeslots size: " + goalTimeslots.size());
            throw new IllegalArgumentException("The reduced query must have only one timeslot");
        }
        q.goalTimeslot = goalTimeslots.first();

        // Set the Goal Role
        if (query._roles.size() == 0) {
            logger.severe("query._roles size: " + query._roles.size());
            throw new IllegalArgumentException("The query must have atleast one Goal Role");
        }

        if (query._roles.size() == 1) {
            q.goalRole = roleHelper.indexOf(query._roles.get(0));
        } else {
            // Create a new role to act as the Goal Role
            Role newGoalRole = roleHelper.getUniqueRole("goalRole");
            roleHelper.add(newGoalRole);
            roleHelper.setupSortedRoles();
            Rule newRule = new Rule(query, newGoalRole);
            rules.addAll(toASAPTimeNSA_Rules(newRule, roleHelper, timeIntervalHelper));

            q.goalRole = roleHelper.indexOf(newGoalRole);
        }

        return q;
    }

    /** @param rule
     * @param roleHelper
     * @param timeIntervalHelper
     * @return */
    public ArrayList<ASAPTimeNSA_Rule> toASAPTimeNSA_Rules(Rule rule, RoleHelper roleHelper,
            TimeIntervalHelper timeIntervalHelper) {
        ArrayList<ASAPTimeNSA_Rule> rules = new ArrayList<ASAPTimeNSA_Rule>();

        SortedSet<Integer> adminInterval = timeIntervalHelper.indexOfReduced(rule._adminTimeInterval);
        SortedSet<Integer> roleSchedule = new TreeSet<Integer>();
        ArrayList<Integer> precondition = new ArrayList<Integer>();

        // Convert RoleSchedule to the New Timeslots
        if (rule._roleSchedule.size() != 0) {
            for (TimeSlot ts : rule._roleSchedule) {
                roleSchedule.addAll(timeIntervalHelper.indexOfReduced(ts));
            }
        } else {
            // Grab all of the reduced timeslots
            roleSchedule = timeIntervalHelper.indexOfReduced(timeIntervalHelper.getAllTimeInterval());
        }

        // Convert Preconditions to the Role Indexes
        for (Role r : rule._preconditions) {
            precondition.add(roleHelper.indexOf(r));
        }

        logger.fine("Timeslots: " + timeIntervalHelper._timeIntervals);
        logger.fine("Reduced Timeslots: " + timeIntervalHelper._reducedTimeIntervals);
        logger.fine("Admin Interval: " + adminInterval);
        logger.fine("Role Schedule: " + roleSchedule);

        ASAPTimeNSA_Rule rule_t;
        for (Integer adminTimeslot : adminInterval) {
            for (Integer roleTimeslot : roleSchedule) {
                rule_t = new ASAPTimeNSA_Rule();

                rule_t.ruleType = rule._type.toRanise();
                if (rule._adminRole.isAllRoles()) {
                    rule_t.adminRole = null;
                } else {
                    rule_t.adminRole = roleHelper.indexOf(rule._adminRole);
                }
                rule_t.adminTime = adminTimeslot;
                rule_t.role = roleHelper.indexOf(rule._role);
                rule_t.precondition = precondition;
                rule_t.roleTime = roleTimeslot;

                logger.fine("Adding rule to rules");

                rules.add(rule_t);
            }
        }

        return rules;
    }

    @Override
    public String getFileExtenstion() {
        return fileExt.ASASPTime_NSA;
    }

}
