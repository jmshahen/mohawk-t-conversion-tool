<! Conversion to ASAPTime NSA Input Format !>
<! !>
CONFIG <numRoles> <numTimeslots>
GOAL <goalRole> <goalTimeslot>
<! !>
<! can_revoke 3 , t3 , true ; t5 , 14 !>
<rules_nsa:{r|<r.ruleType> <if(r.adminRole)><r.adminRole><else>true<endif> , t<r.adminTime> , <if(r.precondition)><r.precondition; separator=" "><else>true<endif> ; t<r.roleTime> , <r.role><\n>}>