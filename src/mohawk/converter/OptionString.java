package mohawk.converter;

public enum OptionString {
    /** Outputs the authors who have contributed code to this project. Does not allow any other actions to occur. */
    AUTHORS("authors"), BULK("bulk"),
    /** Outputs whether NuSMV could be found. Does not allow any other actions to occur. */
    CHECKNUSMV("checknusmv"),
    /** Outputs the help menu. Does not allow any other actions to occur. */
    HELP("help"), LINESTR("linstr"), LOGLEVEL("loglevel"), LOGFILE("logfile"), LOGFOLDER("logfolder"), MAXW("maxw"), NOHEADER("noheader"), ONLYREDUCTION("only"), RESULTSFILE("output"), SPECFILE("input"), SPECEXT("specext"), SHORT_ROLENAMES("shortnames"), TO_ALL("to_all"), TO_ASAPTIME_NSA("to_asaptime_nsa"), TO_ASAPTIME_SA("to_asaptime_sa"), TO_MOHAWK("to_mohawk"),
    /** Takes in a potentially messy Mohawk+T policy and outputs a well nice formatted Mohawk+T policy. */
    TO_MOHAWK_T("to_mohawk_t"),
    /** Converts a Mohawk+T policy to a NuSMV/NuXMV model. */
    TO_NUSMV("to_nusmv"), TO_TROLE("to_trole"), TO_TRULE("to_trule"),
    /** Outputs the current version of this code repository. Does not allow any other actions to occur. */
    VERSION("version");

    private String _str;

    private OptionString(String s) {
        _str = s;
    }

    @Override
    public String toString() {
        return _str;
    }

    /** Returns the commandline equivalent with the hyphen and a space following: AUTHORS -> "-authors "
     * 
     * @return */
    public String c() {
        return "-" + _str + " ";
    }

    /** Returns the commandline equivalent with the hyphen and a space following: LOGLEVEL("debug") -> "-loglevel debug
     * "
     * 
     * @param param
     * @return */
    public String c(String param) {
        return "-" + _str + " " + param + " ";
    }
}
