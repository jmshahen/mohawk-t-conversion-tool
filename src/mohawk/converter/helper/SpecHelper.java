package mohawk.converter.helper;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

import mohawk.global.helper.ParserHelper;
import mohawk.global.parser.BooleanErrorListener;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;

public class SpecHelper {
    public final static Logger logger = Logger.getLogger("mohawk");
    public String smvFilepath = "latestRBAC2SMV.smv";
    public Boolean smvDeleteFile = false;
    public String specFile = "";
    public String fileExt = ".spec";
    public ArrayList<File> specFiles = new ArrayList<File>();
    public boolean bulk = false;
    public Long TIMEOUT_SECONDS = (long) 120; // Default 2 minutes

    public BooleanErrorListener error = new BooleanErrorListener();

    public void loadSpecFiles() throws IOException {
        if (this.bulk == true) {
            this.loadSpecFilesFromFolder(this.specFile);
        } else {
            this.addSpecFile(this.specFile);
        }
    }

    public void loadSpecFilesFromFolder(String path) {
        if (path == null || path == "") {
            logger.severe("[ERROR] No SPEC Folder provided");
        }

        File folder = new File(path);

        if (!folder.exists()) {
            logger.severe("[ERROR] Spec Folder: '" + path + "' does not exists!");
        }
        if (!folder.isDirectory()) {
            logger.severe("[ERROR] Spec Folder: '" + path + "' is not a folder and the 'bulk' option is present. "
                    + "Try removing the '-bulk' option if you wish to use "
                    + "a specific file, or change the option to point to a folder");
        }

        for (File f : folder.listFiles()) {
            if (f.getName().endsWith(fileExt)) {
                logger.fine("Adding file to specFiles: " + f.getAbsolutePath());
                specFiles.add(f);
            }
        }
    }

    public void addSpecFile(String path) throws IOException {
        if (path == null || path.equals("")) {
            logger.severe("[ERROR] No SPEC File provided");
        }

        File file2 = new File(path);
        if (!file2.exists()) {
            logger.severe("[ERROR] Spec File: '" + path + "' does not exists!");
        }
        if (!file2.isFile()) {
            logger.severe("[ERROR] Spec File: '" + path + "' is not a file and the 'bulk' option was not set. "
                    + "Try setting the '-bulk' option if you wish to search "
                    + "through the folder, or change the option to point to a specific file");
        }

        logger.info("[FILE IO] Using SPEC File: " + file2.getCanonicalPath());
        specFiles.add(file2);
    }

    public File getSmvFile(File specFile2) {
        File nusmvFile = null;
        if (smvDeleteFile) {
            try {
                nusmvFile = File.createTempFile("smvTempFile", ".smv");
            } catch (IOException e) {
                logger.severe("[ERROR] Unable to create a temporary SMV file in the current working directory: "
                        + new File("").getAbsoluteFile());
            }
            return nusmvFile;
        }

        if (bulk) {
            nusmvFile = new File(
                    new File(smvFilepath).getAbsolutePath() + File.pathSeparator + specFile2.getName() + ".smv");
        } else {
            nusmvFile = new File(smvFilepath);
        }

        if (!nusmvFile.exists()) {
            try {
                nusmvFile.createNewFile();
            } catch (IOException e) {
                logger.severe("[ERROR] Unable to create a SMV file in the current working directory: "
                        + new File("").getAbsoluteFile());
            }
        }
        return nusmvFile;
    }

    public MohawkTARBACParser parseMohawkTFile(File specFile) throws IOException {
        FileInputStream fis = new FileInputStream(specFile);

        error.errorFound = false; // reset the error listener
        MohawkTARBACParser parser = ParserHelper.runParser(fis, error, false);

        if (error.errorFound) {
            logger.warning("Unable to parse the file: " + specFile.getAbsolutePath());
        }

        return parser;
    }
}
