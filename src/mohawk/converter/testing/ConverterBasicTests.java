package mohawk.converter.testing;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import mohawk.global.pieces.Role;
import mohawk.global.pieces.Rule;
import mohawk.global.pieces.RuleType;
import mohawk.global.pieces.TimeSlot;

import org.junit.Test;

public class ConverterBasicTests {

    @Test
    public void test_toMohawk_Query() {
        ArrayList<Rule> rules = new ArrayList<Rule>();

        testFunc1(rules);
        assertEquals(1, rules.size());
    }

    private void testFunc1(ArrayList<Rule> rules) {
        Rule newRule = new Rule();
        newRule._type = RuleType.ASSIGN;
        newRule._adminRole = Role.allRoles();
        newRule._adminTimeInterval = new TimeSlot(1);
        newRule._preconditions.add(new Role("test"));
        newRule._roleSchedule = new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(1)));
        newRule._role = new Role("testGoalRole");

        rules.add(newRule);
    }

}
