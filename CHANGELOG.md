# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

#####################################################
## [1.3.0] - 2015-11-09
### Fixed
- Remove CanEnable/CanDisable reduction has been fixed
- Removed large debugging statements from running by surrounding them with an IF statement (big performance increase)
- Lots of code cleanups and speedups

### Bugs
- Still differing errors for AGTUniv04, AGTUniv08-10, AGTHos08-10 from ASASPTime NSA

### Added
- New field added to Role: 
- New Option to ONLY execute the Remove CanEnable v1 or the CanEnable v2 Reduction (-only)
- Can now reduce rolenames for conversion to Mohawk (instead of role1_ENABLED_AL0_u12, we get r2) 
	(big performance increase)

#####################################################
## [1.2.0] - 2015-08-16
### Fixed
- Fixed conversion error which produced a system that was not always correct

### Bugs
- AGTUniv10 takes a really long time, and a lot of refinement steps; 
each refinement step is quick, so the abstraction-refinement process might need to updated

### Added
- CHANGELOG
- VERSION


